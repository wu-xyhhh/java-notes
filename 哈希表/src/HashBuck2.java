import java.util.HashMap;
import java.util.Objects;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-02-24
 * Time: 22:04
 */
class Person{
    public String Id;

    public Person(String id) {
        this.Id = id;
    }

    @Override
    public String toString() {
        return "Person{" +
                "Id='" + Id + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(Id, person.Id);
    }

    @Override
    public int hashCode() {//hashCode必须和equals一起重写
        return Objects.hash(Id);
    }
}
public class HashBuck2 {
    //不重写equals和hashcode时候
    public static void main0(String[] args) {
        //hashcode
        Person person1=new Person("123");
        Person person2=new Person("123");
        //假设接下来的key是Person，Id表示他的身份证
        //如果Id相同，则认为是一个人

        //现在如果要把Person1和Person2放到散列表中
        //由于Person是一个引用类型，我们这里要用到hashcode
        //hashcode()调用完,会生成一个整数x，x%length=index
        //得到的index应该是一样的

        System.out.println(person1.hashCode());
        System.out.println(person2.hashCode());//打印出的整数并不一样
    }

    //重写equals和hashcode时候
    public static void main(String[] args) {
        Person person1=new Person("123");
        Person person2=new Person("123");


        System.out.println(person1.hashCode());
        System.out.println(person2.hashCode());//打印相同的整形
        //为什么呢？因为"123"是引用类型，像这种自定义的类型，你必须要重写hashcode和equals才能实现逻辑
        HashMap<Person,String> map=new HashMap<>();
    }
}
