/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-02-23
 * Time: 21:47
 */
//力扣387
class Solution {
    public int firstUniqChar(String s) {
        char[]arr=new char[26];//最多26个不同字母
        int i=0;
        for(i=0;i<s.length();i++){
            char x=s.charAt(i);
            arr[x-97]++;//该位置记录了1次
        }
        for(i=0;i<s.length();i++){
            char x=s.charAt(i);
            if(arr[x-97]==1){
                return i-1;
            }
        }
        return -1;//走完上面的for还没结束，说明不存在
    }
}
public class TestDemo {
}
