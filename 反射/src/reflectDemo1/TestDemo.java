package reflectDemo1;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-03-01
 * Time: 17:58
 */
class Student{
    //私有属性name
    private String name = "bit";
    //公有属性age
    public int age = 18;
    //不带参数的构造方法
    public Student(){
        System.out.println("Student()");
    }
    private Student(String name,int age) {
        this.name = name;
        this.age = age;
        System.out.println("Student(String,name)");
    }
    private void eat(){
        System.out.println("i am eat");
    }
    public void sleep(){
        System.out.println("i am pig");
    }
    private void function(String str) {
        System.out.println(str);
    }
    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
public class TestDemo {

    //获取class对象的3种方式
    public static void main0(String[] args) throws ClassNotFoundException {
        //要反射学生对象，需要先获取学生类的class对象

        //方法一：使用 Class.forName("类的全路径名"); 静态方法。
        Class<?> c1=Class.forName("reflectDemo1.Student");//我这里Student类是放在reflectDemo1的包下面
        //Class.forName方法会抛出一个一次，这里我们在main函数那里抛出一下
        //Class.forName方法返回值是Class<?>,我们这里用c1接收一下


        //方法二：使用 .class 方法
        Class<?> c2=Student.class;


        //方法三：使用类对象的 getClass() 方法  ps：第三种方法用的比较少
        Student student=new Student();
        Class<?> c3= student.getClass();

        //测试我们获得的c1，c2，c3是否相同
        System.out.println(c1==c2);
        System.out.println(c1==c3);
        System.out.println(c3==c2);//都打印true
        //说明不管你用什么方式来获得class对象，我们的class对象只有一个

    }



}
