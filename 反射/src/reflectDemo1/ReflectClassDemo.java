package reflectDemo1;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-03-01
 * Time: 18:19
 */

public class ReflectClassDemo {

    //通过Class类的newInstance方法获取学生的实例
    public static void reflectNewInstance() throws ClassNotFoundException {
        try {
            Class<?> c1=Class.forName("reflectDemo1.Student");
            //通过Class类的newInstance方法，获取学生实例
            Student student=(Student) c1.newInstance();
            System.out.println(student);//打印Student{name='bit', age=18}
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    // 反射私有的构造方法 屏蔽内容为获得公有的构造方法
    public static void reflectPrivateConstructor() throws ClassNotFoundException {
        try{
            Class<?> c1=Class.forName("reflectDemo1.Student");
            Constructor<?> constructor= c1.getDeclaredConstructor(String.class,int.class);
            //这里Construct<>里用？，因为c1也没有指定是Student，写？就行了

            constructor.setAccessible(true);
            //因为是私有属性，这里默认是false，你需要手动设为true才有访问权限
            Student student=(Student) constructor.newInstance("小黑",18);
            System.out.println(student);//打印Student{name='小黑', age=18}
        }catch (ClassNotFoundException  e){
            e.printStackTrace();
        }catch (NoSuchMethodException e){
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    // 反射私有（或共有）属性
    public static void reflectPrivateField() {
        try{
            Class<?> c1=Class.forName("reflectDemo1.Student");

            Student student=(Student) c1.newInstance();

            Field field=c1.getDeclaredField("name");
            field.setAccessible(true);

            field.set(student,"小白");
            System.out.println(student);//打印Student{name='小白', age=18}
        }catch (ClassNotFoundException  e){
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }


    }

    // 反射私有方法
    public static void reflectPrivateMethod() {
        try{
            Class<?> c1= Class.forName("reflectDemo1.Student");

            Student student=(Student) c1.newInstance();

            Method method=c1.getDeclaredMethod("function", String.class);
            method.setAccessible(true);

            method.invoke(student,"我是一个私有构造方法参数");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws ClassNotFoundException {
        //reflectNewInstance();
        //reflectPrivateConstructor();
        //reflectPrivateField();
        reflectPrivateMethod();
    }

}
