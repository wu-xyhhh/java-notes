import java.io.IOException;
import java.util.Random;
import java.util.Scanner;
public class Test {
    public static void main1(String[] args) {
        System.out.println("输出且换行");
        System.out.print("输出且不换行");//print和println的区别就是c语言中printf加不加\n的区别
        System.out.printf("%d\n",10);//和C语言的printf类似
    }

    public static void main2(String[] args) throws IOException {//比较罕见的方法（了解即可）
        System.out.println("enter a char:");
        char i=(char)System.in.read();//这里以读取char类型为例，你也可以读取其他类型，强转一下即可
        System.out.println("your char is:"+i);
    }

    public static void main3(String[] args) {
        Scanner scanner=new Scanner(System.in);
        //用Scanner之前需要import java.util.Scanner;就类似于你C语言用printf要先#include<stdio.h>一样
        //参数System.in表示从键盘输入
        int n=scanner.nextInt();
        System.out.println(n);
        //这里是用int型的n接收scanner过来的数据
        // 如果需要其他类型接收，比如long 型的b，代码按如下方式更改：
        long b=scanner.nextLong();//其他类型以此类推,你要读入什么类型的数据，就next啥
        System.out.println(b);
        //关于读入字符串
        String str= scanner.nextLine();//这里稍微与上面的有些不同，也很好记住，读入一串字符就是读一行嘛
        System.out.println(str);
        //但是需要注意的，运行的时候直接“就没给我机会输入字符串”，为什么呢？
        //解释：这里是读入字符串，你在上一个打印b，按了一个回车，电脑会以为你想接收一个回车，所以已经接收完了，不会给你机会再输入了
        //所以！！！这里非常推荐你输入字符串要放在输入其他类型前面，否则非常容易出错

    }

    //关于字符串的一些注意事项
    public static void main4(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String str=scanner.next();//这里我们输入“哈哈 你好”
        System.out.println(str);//只打印了哈哈
        //解释：scanner.next()和 scanner.nextLine()的区别是，前者读到空格就停止了，后者可以读空格及后面的字符串
        scanner.close();
        //java中scanner就是类似于文件的东西，你用了这个scanner你的相关文件是属于打开状态，用完可以关闭一下
        //当然你要是不关闭也没关系，程序运行结束也会自动关闭
    }

    //循环读取n个数据
    public static void main5(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while(scanner.hasNextInt())//idea中按ctrl+d结束读取
        {//这里以读取整数为例，如果你想循环读取其他数据类型,while(scanner.hasNext类型名（））即可
            int n=scanner.nextInt();
            System.out.println(n);
        }
    }
    //ps:怎么看scanner.hasNextInt();中hasNextInt的返回类型
    //按住ctrl，然后点击hasNextInt即可


    //获取一个二进制序列中的所有偶数位和奇数位，分别输出二进制序列
    //eg:2的二进制序列位10 偶数位：1 奇数位：0
    //   4的二进制序列位100 偶数位：0 奇数位：1 0
    public static void main6(String[] args) {
         Scanner scanner=new Scanner(System.in);
         int n=scanner.nextInt();
         int []arr=new int[16];//用于存放奇数位，一个数32位，奇/偶数位最高16位
         int []brr=new int [16];//用于存放偶数位
         int i=0;
         int j=0;
         int k=0;
         for(i=1;i<=32;i++)
         {
             if(i%2!=0)//奇数位
             {
                 arr[j]=n&1;
                 //比如n=1010，n&1
                 //   1=0001
                 // n&1=0000=0，获取到了n的末位
                 j++;
                 n>>>=1;//无符号右移
             }
             else
             {
                 brr[k]=n&1;
                 k++;
                 n>>>=1;
             }
         }
        System.out.println("现打印奇数位：");
         for(i=0;i<16;i++)
         {
             System.out.print(arr[15-i]+" ");
         }
        System.out.println();
        System.out.println("现打印偶数位：");
        for(i=0;i<16;i++)
        {
            System.out.print(brr[15-i]+" ");
        }
    }

    //输出一个整数的每一位
    public static void main7(String[] args) {
        int n=0;
        Scanner scanner=new Scanner(System.in);
        n=scanner.nextInt();
        int count=0;
        int tmp=n;
        while(tmp!=0)//计算n是几位数
        {
            count++;
            tmp/=10;
        }
        int i=0;
        int []arr=new int[count];
        for(i=0;i<count;i++)
        {
            arr[i]=n%10;//计算末位
            n/=10;
        }
        for(i=0;i<count;i++)
        {
            System.out.print(arr[count-1-i]+" ");
        }
    }

    //输出乘法口诀表
    //输出n*n的乘法口诀表，n由用户输入。
    public static void main8(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        int i=0;
        for(i=1;i<=n;i++)//打印n行
        {
            int j=0;
            for(j=1;j<=i;j++)//每行式子个数
            {
                System.out.printf("%d*%d=%d ",i,j,i*j);
            }
            System.out.println();
        }
    }

    //猜数字游戏
    public static void main9(String[] args) {
        Random random=new Random();
        int rand=random.nextInt(100);//随机生成一个数，[0-100）范围内，也就是生成0-99的整数
        Scanner scanner=new Scanner(System.in);
        while(true)//因为大概率没办法一次猜中，如果想多猜，这里while用true判断，我们后面猜中break即可
        {
            int n=scanner.nextInt();
            if(n<rand)
            {
                System.out.println("猜小啦！");
            }
            else if(n>rand)
            {
                System.out.println("猜大啦");
            }
            else
            {
                System.out.println("你猜对了！");
                break;
            }
        }
    }

    //改良版：判断一个数是不是素数
    public static void main10(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        int i=0;
        for(i=2;i<=Math.sqrt(n);i++)
        {
            if(n%i==0)
            {
                System.out.println(n+"不是素数");
                break;
            }
        }
        if(i>Math.sqrt(n))
        {
            System.out.println(n+"是素数");
        }
    }


    //打印1000-2000闰年问题
    public static void main11(String[] args) {
        int i=0;
        for(i=1000;i<=2000;i++)
        {
            if(i%4==0&&i%100!=0)
            {
                System.out.println(i+"是闰年");
            }
            else if(i%400==0)
            {
                System.out.println(i+"是闰年");
            }
        }
    }

    //方法
    public static int Add(int a,int b) {//a和b是形参
    return a+b;
    }

    public static void cheng2(int a) {
        a=a*2;
    }
    public static void main12(String[] args) {
        int c=Add(3,6);//3和6实参
        System.out.println(c);
        System.out.println(Add(3,6));//这样直接打印也可
        int b=1;
        cheng2(b);
        System.out.println(b);
    }

    //方法实战1!+2!+3!+...n!
    public static int jc(int x) {
        int ret=1;
        int i=0;
        for(i=1;i<=x;i++)
        {
            ret=ret*i;
        }
        return ret;
    }
    public static void main13(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        int i=0;
        int j=1;
        int sum=0;
        for(i=1;i<=n;i++)//1!+2!+...n!要进行n次加法
        {
            j=jc(i);
            sum+=j;
        }
        System.out.println(sum);
    }


    public static int add(int a,int b) {
        return a+b;
    }

    public static double add(double a,double b) {//java中函数名是可以相同的，重要的是函数的声明不能一样,和返回值没有关系
        return a+b;
    }

    public static void main14(String[] args) {
        int a=1;
        int b=2;
        double c=3.14;
        double d=1.526;
        int e=add(a,b);
        double f=add(c,d);
        System.out.println(e);//打印3
        System.out.println(f);//打印4.666
    }
    //重载：overload
    //1.方法名相同
    //2.方法的参数列表不同（个数，类型）
    //3.方法的返回值不作要求


    //题目：编写代码模拟三次密码输入的场景。
    // 最多能输入三次密码，密码正确，提示“登录成功”,密码错误，可以重新输入
    // 最多输入三次。三次均错，则提示退出程序
    public static void main15(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int count=3;//计算剩余可输入次数
        while(count!=0)
        {
            System.out.println("请输入密码：");
            String password=scanner.nextLine();
            if(password.equals("12345"))//java中判断字符串是否相同用字符串.equals("..."),相同则返回true
            {
                System.out.println("登录成功");
                break;
            }
            else//输入错误
            {
                count--;//剩余可输入次数减一
                System.out.println("输入错误，请重新输入,你还有"+count+"次机会");
            }
        }
    }

    public static double add(double a,double b,double c) {
        return a+b+c;
    }

    public static void main16(String[] args) {
        int a=1;
        int b=2;
        double c=3.14;
        double d=1.53;
        double e=2.31;
        int f=add(a,b);
        double g=add(c,d,e);
        System.out.println(f);
        System.out.println(g);
    }

    public static int bj(int a,int b) {
        return a>b?a:b;
    }

    public static double bj(double a,double b) {
         return a>b?a:b;
    }

    public static void bj(double a,double b,int c) {
        if(a>b)
        {
            if(c>(int)a)//c>a>b
            {
                System.out.println(c+">"+a+">"+b);
            }
            else if(c<=(int)b)//a>b>c
            {
                System.out.println(a+">"+b+">"+c);
            }
            else//a>c>b
            {
                System.out.println(a+">"+c+">"+b);
            }
        }
        else if(b>a)
        {
            if(c>(int)b)//c>b>a
            {
                System.out.println(c+">"+b+">"+a);
            }
            else if(c<=(int)a)//b>a>c
            {
                System.out.println(b+">"+a+">"+c);
            }
            else//b>c>a
            {
                System.out.println(b+">"+c+">"+a);
            }
        }
    }

    public static void main17(String[] args) {
        int x=bj(3,4);
        System.out.println(x);
        double y=bj(3.14,2.16);
        System.out.println(y);
        bj(1.1,2.3,6);
    }

    public static void bj3(int a,int b,int c) {
       int d=bj( bj(a,b),c);
        System.out.println(d);
    }
    public static void main18(String[] args) {
        bj3(1,6,2);
    }


    //找出出现一次的数字
    //异或法（最优）
    public static void lc(int []nums) {
        int a = nums[0];
        for(int i=1;i<nums.length;i++) {
            a = a^nums[i];
            //一个数字异或它自己结果为0,异或其他数不为0
            if(a==0)
            {
                System.out.println(nums[i]);
            }
        }
    }
    public static void main19(String[] args) {
        int []arr={1,2,3,3,4};
        lc(arr);
    }


    //奇数位于偶数之前
    public static void jo(int [] array) {
        int length = array.length;
        int i=0;
        for( i = 0; i < length; i ++){
            for(int j = length - 1; j > 0; j --){
                if(array[j] % 2 == 1 && array[j - 1] % 2 == 0){
                    int temp = array[j];
                    array[j] = array[j - 1];
                    array[j - 1] = temp;
                }
            }
        }
        for(i=0;i<length;i++)
        {
            System.out.print(array[i]+" ");
        }
    }

    public static void main20(String[] args) {
        int []arr={1,9,6,8,7,2};
        jo(arr);
    }


    //求斐波那契数列的第n个数
    public static int fibNaQie(int n) {

        if(n==1)
        {
            return 0;
        }
        else if(n==2)
        {
            return 1;
        }
        else
        {
          return fibNaQie(n-1)+fibNaQie(n-2);
        }
    }

    public static void main(String[] args) {
        System.out.println(fibNaQie(1));
        System.out.println(fibNaQie(2));
        System.out.println(fibNaQie(3));
        System.out.println(fibNaQie(4));
        System.out.println(fibNaQie(5));
    }
}

