/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-11-27
 * Time: 11:19
 */
class NameException extends RuntimeException{
    //自己自定义的一个异常必须继承一个异常，
    //比如我们这里自定义的姓名异常继承了RuntimeException
    public NameException(String message){
        super(message);
    }
}
class PasswordException extends RuntimeException{
    public PasswordException(String message){
        super(message);
    }
}
public class TestDemo {
    /*private static String name="bit";
    private static String password="123";

    public static void login(String name,String password) {
       if(!TestDemo.name.equals(name)){
           throw new NameException("用户名输入错误");
       }
       if(!TestDemo.password.equals(password)){
           throw new NameException("密码输入错误");
       }
    }

    public static void main(String[] args) {
        login("bit","1234");
    }*/

    private static void testMethod(){

        System.out.println("testMethod");

    }

    public static void main(String[] args) {

        ((TestDemo)null).testMethod();

    }
}
