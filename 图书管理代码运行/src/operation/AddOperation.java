package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-11-20
 * Time: 19:04
 */
//这里类也可以写在BookList里面，这里是单独拎出来了
public class AddOperation implements IOperation{//添加
    public void work(BookList bookList){
        Scanner scanner=new Scanner(System.in);
        System.out.println("正在新增图书！");
        System.out.println("请输入书名：");
        String name=scanner.nextLine();
        System.out.println("请输入作者名：");
        String author=scanner.nextLine();
        System.out.println("请输入价格：");
        int price=scanner.nextInt();
        System.out.println("请输入书的类型");
        String type=scanner.nextLine();
        Book book=new Book(name,author,price,type);
        int size=bookList.getUsedSize();//看一下现在书架上有几本书了，返回使用量给size
        bookList.setBooks(size,book);//书架书的下标从0开始计算，假设有n本书，则第n+1本下标正好为n
        bookList.setUsedSize(size+1);//放了一本书，书架上 书的总量+1
        System.out.println(name+"已被添加！");
    }
}
