package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-11-20
 * Time: 19:05
 */
public class BorrowOperation implements IOperation{
    public void work(BookList bookList){
        Scanner scanner=new Scanner(System.in);
        System.out.println("借阅图书！");
        System.out.println("请输入你要借阅的书的名字：");
        String name=scanner.nextLine();
        int size= bookList.getUsedSize();
        for(int i=0;i<size;i++){
            Book book= bookList.getPos(i);
            if(name.equals(book.getName())){
                book.setBorrowed(true);
                System.out.println("借阅成功，到期请及时归还");
                return;
            }
        }
        System.out.println("抱歉，没有你要借阅的图书");
    }
}
