package operation;

import book.BookList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-11-20
 * Time: 19:06
 */
public class ExitOperation implements IOperation{
    public void work(BookList bookList){
        System.out.println("系统已退出！");
    }

}
