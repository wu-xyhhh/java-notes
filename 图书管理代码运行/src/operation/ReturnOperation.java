package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-11-20
 * Time: 19:06
 */
public class ReturnOperation implements IOperation{
    public void work(BookList bookList){
        Scanner scanner=new Scanner(System.in);
        System.out.println("图书归还！");
        System.out.println("请输入你要归还的图书");
        String name=scanner.nextLine();
        int size= bookList.getUsedSize();
        for(int i=0;i<size;i++){
            Book book= bookList.getPos(i);
            if(name.equals(book.getName())){
                book.setBorrowed(false);
                System.out.println("图书已归还");
                System.out.println(book);
                return;
            }
        }
        System.out.println("抱歉，系统未记录你要归还的图书");
    }
}
