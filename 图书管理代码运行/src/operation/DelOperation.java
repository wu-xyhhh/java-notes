package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-11-20
 * Time: 19:04
 */
public class DelOperation implements IOperation{
    public void work(BookList bookList){
        Scanner scanner=new Scanner(System.in);
        System.out.println("删除图书！");
        System.out.println("请输入删除的图书书名");
        String name=scanner.nextLine();
        int currentSize= bookList.getUsedSize();
        int index=0;//存储找到的下标
        int i=0;
        for( i=0;i<currentSize;i++){
            Book book=bookList.getPos(i);
            if(book.getName().equals(name)){
                index=i;
                break;
            }
        }
        if(i>=currentSize){
            System.out.println("未找到要删的书");
            return;
        }
        //进行删除
        for(int j=index;j<currentSize-1;j++){
            Book book= bookList.getPos(j+1);
            bookList.setBooks(j,book);
        }
        bookList.setBooks(currentSize,null);
        bookList.setUsedSize(currentSize-1);
        System.out.println("删除成功");
    }
}
