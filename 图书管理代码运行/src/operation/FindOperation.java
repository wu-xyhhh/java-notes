package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-11-20
 * Time: 19:05
 */
public class FindOperation implements IOperation{
    public void work(BookList bookList){
        Scanner scanner=new Scanner(System.in);
        System.out.println("查找图书！");
        String name=scanner.nextLine();
        int size=bookList.getUsedSize();
        for(int i=0;i<size;i++){
            Book book= bookList.getPos(i);
            if(name.equals(book.getName())){
                System.out.println("该书已被系统找到,信息如下:");
                System.out.println(book);
                return;
            }
        }
        System.out.println("未找到该书");
    }
}
