package operation;

import book.Book;
import book.BookList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-11-20
 * Time: 19:05
 */
public class DisplayOperation implements IOperation{
    public void work(BookList bookList){
        System.out.println("打印图书！");
        int size= bookList.getUsedSize();
        for(int i=0;i<size;i++){
            Book book=bookList.getPos(i);//这里不能用bookList[i],bookList这里不是数组啊
            System.out.println(book);
        }
    }
}
