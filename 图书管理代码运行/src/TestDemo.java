/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-11-20
 * Time: 17:34
 */
class A{
    static{
        System.out.println("A的静态代码块执行了");
    }
    {
        System.out.println("A的实例代码块执行了");
    }
    public A(){
        System.out.println("A的构造方法执行了");
    }
}
class B extends A{
    static{
        System.out.println("B的静态代码块执行了");
    }
    {
        System.out.println("B的实例代码块执行了");
    }
    public B(){
        System.out.println("B的构造方法执行了");
    }
}
public class TestDemo {
    public static void main(String[] args) {
        B b=new B();
    }
}
