package user;

import book.BookList;
import operation.IOperation;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-11-20
 * Time: 19:15
 */
public abstract class User  {//包含管理员和普通使用者
    protected String name;
    protected IOperation[] iOperations;
    public User(String name){
        this.name=name;
    }
    public abstract int menu();//给子类动态绑定的(子类已重写menu)，这里没有其他实际的作用
    //没有实际作用的方法，我们把它变成抽象方法
    //变成抽象方法后，类也要改成抽象类

    public void doWork(int choice, BookList bookList){
        iOperations[choice].work(bookList);
        //iOperations是一个操作数组，iOperations[choice]获取一个（数组内的）对象
        //.work(bookList)是通过对象进行work操作（书架）
    }
}
