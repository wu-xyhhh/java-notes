package user;

import operation.*;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-11-20
 * Time: 19:17
 */
public class NormalUser extends User{//普通用户
    public NormalUser(String name){
        super(name);
        this.iOperations=new IOperation[] {
                new ExitOperation(),
                new FindOperation(),
                new BorrowOperation(),
                new ReturnOperation()

        };
    }

    public int menu(){
        System.out.println("============用户菜单============");
        System.out.println("hello "+this.name+"!");
        System.out.println("请输入对应选项以进行操作");
        System.out.println("1.查找图书");
        System.out.println("2.借阅图书");
        System.out.println("3.归还图书");
        System.out.println("0.退出系统");
        System.out.println("================================");
        Scanner scanner=new Scanner(System.in);
        int choice=scanner.nextInt();
        return choice;
    }
}
