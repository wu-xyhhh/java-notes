package user;

import operation.*;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-11-20
 * Time: 19:16
 */
public class AdminUser extends User{//管理员
    public AdminUser(String name){
      super(name);
      this.iOperations=new IOperation[] {
              new ExitOperation(),//下标0
              new FindOperation(),//1
              new AddOperation(),//2
              new DelOperation(),//3
              new DisplayOperation(),//4
              //下标顺序对应菜单中的操作
      };
    }
    public int menu(){
        System.out.println("============管理员菜单============");
        System.out.println("hello "+this.name+"!");
        System.out.println("请输入对应选项以进行操作");
        System.out.println("1.查找图书");
        System.out.println("2.添加图书");
        System.out.println("3.删除图书");
        System.out.println("4.显示图书");
        System.out.println("0.退出系统");
        System.out.println("================================");
        Scanner scanner=new Scanner(System.in);
        int choice=scanner.nextInt();
        return choice;
    }
}
