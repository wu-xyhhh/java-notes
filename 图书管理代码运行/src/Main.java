import book.BookList;
import user.AdminUser;
import user.NormalUser;
import user.User;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-11-20
 * Time: 19:27
 */
public class Main {//程序入口点

    public static User login() {
        System.out.println("请输入你的姓名：");
        Scanner scanner=new Scanner(System.in);
        String name=scanner.nextLine();
        System.out.println("请输入你的身份：1.管理员 2.用户");
        int choice=scanner.nextInt();
        if(choice==1){
            return  new AdminUser(name);
        }else{
            return new NormalUser(name);
        }
    }
    public static void main(String[] args) {
        User user=login();//login()会返回一个使用者（管理员/用户），赋给User（向上转型）
        //赋值过去时，假设传的是1,那么AdminUser里面的就会创建一个包含 “相关操作”（比如查找图书）对象 的数组
        BookList bookList=new BookList();//创建好一个书架
        while(true){
            int choice =user.menu();//动态绑定（多态）
            //根据choice调用合适操作
            user.doWork(choice,bookList);
        }
    }
}
