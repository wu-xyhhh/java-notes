package book;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-11-20
 * Time: 18:36
 */
public class BookList {//书架
    private Book[] books=new Book[10];
    private int usedSize;//书架放了几本书,不赋值，默认为0
    public BookList(){//先放三本，将来有需要再添加
        books[0]=new Book("三国演义","罗贯中",16,"小说");
        books[1]=new Book("水浒传","施耐庵",61,"小说");
        books[2]=new Book("西游记","吴承恩",19,"小说");
        this.usedSize=3;
    }

    public int getUsedSize(){
        return usedSize;
    }

    public void setUsedSize(int usedSize){
        this.usedSize=usedSize;
    }

    public Book getPos(int pos){//找到书的位置（下标）
        return this.books[pos];
    }

    public void setBooks(int pos,Book book){
        this.books[pos]=book;
    }
}
