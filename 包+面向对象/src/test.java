/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-11-18
 * Time: 15:44
 */
interface IA{
    void funcA();
}
interface IB extends IA{//接口IB通过继承，也拥有了接口IA的功能
    void funcB();
}
class Ic implements IB{
    @Override
    public void funcB() {

    }//只重写一个funcB会报错，因为你IB也继承了IA

    @Override
    public void funcA() {

    }
}


class Animal {
    protected String name;

    public Animal(String name) {
        this.name = name;
    }
}
//不是所有动物都会飞、游泳等，我们不能直接把这些特性写到Animal类中
//一个类不能同时继承多个类，但可以实现多个接口
interface IFlying {
    void fly();
}
interface IRunning {
    void run();
}
interface ISwimming {
    void swim();
}


class Cat extends Animal implements IRunning {
    public Cat(String name) {
        super(name);
    }
    @Override
    public void run() {
        System.out.println(this.name + "正在用四条腿跑");
    }
}

class Fish extends Animal implements ISwimming {
    public Fish(String name) {
        super(name);
    }
    @Override
    public void swim() {
        System.out.println(this.name + "正在用尾巴游泳");
    }
}

class Duck extends Animal implements IRunning, ISwimming, IFlying {
    public Duck(String name) {
        super(name);
    }
    @Override
    public void fly() {
        System.out.println(this.name + "正在用翅膀飞");
    }
    @Override
    public void run() {
        System.out.println(this.name + "正在用两条腿跑");
    }
    @Override
    public void swim() {
        System.out.println(this.name + "正在漂在水上");
    }
}

public class test {





    public static void running(IRunning iRunning) {
        iRunning.run();
    }

    public static void swimming(ISwimming iSwimming) {
        iSwimming.swim();
    }
    public static void flying(IFlying iFlying){
        iFlying.fly();
    }

    public static void main0(String[] args) {
        running(new Duck("鸭子"));
        flying(new Duck("鸭子"));
        swimming(new Fish("鱼"));
    }
}
