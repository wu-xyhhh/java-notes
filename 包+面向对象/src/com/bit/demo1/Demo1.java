package com.bit.demo1;
class Name{
    private String name;
    //一旦属性或方法被private修饰，则该属性或方法就被封装起来了
    //封装的效果就是：被封装的属性或方法只能在当前的类中使用
    public void setName(String name){
        this.name=name;
        //this表示当前对象的引用
    }
}

class Animal{
    public String name;
    public int age;
    protected int count;

    public Animal(String name,int age){
            this.name=name;
            this.age=age;
    }

    public void eat(){
        System.out.println(name+"正在吃东西");
    }
}
class Dog extends Animal{
    public Dog(String name,int age){
    super(name,age);
    //super：显示调用构造方法
    // 这里的super（name,age）就是调用父类的两个参数构造方法
    }
    /*public void eat(){
        System.out.println(name+"正在吃骨头");
    }*/
}
class Bird extends Animal{
    public Bird(String name,int age){
        super(name,age);
    }
    public String wing;//翅膀
    public void fly(){
        System.out.println("正在飞行");
    }
}
class B {
    public int Func() {
        System.out.print("B");
        return 0;
    }
}
class D extends B {
    @Override
    public int Func() {
        System.out.print("D");
        return 0;
    }
}
public class Demo1 {
    /*public static void func(Animal animal){

    }
    public static Animal func2(){
        Dog dog=new Dog("xiaoH",1);
        return dog;
    }*/

    public static void main(String[] args) {
        B a = new B();
        B b = new D();
        a.Func();
        b.Func();
    }
    public static void main1(String[] args) {
        Animal animal=new Dog("xiaoHei",1);
        //func(dog);
        Dog dog=(Dog)animal;
        dog.eat();
    }




    int val=1;//不加任何访问修饰符，默认是包访问权限
    public static void main0(String[] args) {
        Dog d=new Dog("xiaoHei",1);
        System.out.println(d.name);
        System.out.println(d.count);
    }
}
