package com.bit.demo1;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-11-19
 * Time: 14:22
 */
class Student implements Comparable<Student>{
    public int age;
    public String name;
    public double score;

    public Student(int age, String name, double score) {
        this.age = age;
        this.name = name;
        this.score = score;
    }

    @Override
    public String toString() {
        return "Student{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", score=" + score +
                '}';
    }

    @Override
    public int compareTo(Student o) {
        if(this.score>o.score){//哪个对象调用CompareTo，哪个对象就是this
            //比如s1.compareTo(s2)，s1就是this，s2就是0
            return 1;
        }else if(this.score==o.score){
            return 0;
        }else{
            return -1;
        }
    }
}
public class Demo3 {
    public static void main0(String[] args) {
        int[]array={1,21,3,13,5,26};
        System.out.println(Arrays.toString(array));
        Arrays.sort(array);//通过Arrays.sort对整个数组进行排序
        System.out.println(Arrays.toString(array));
    }

    public static void main(String[] args) {
        Student[] student=new Student[3];
        student[0]=new Student(20,"张三",86.7);
        student[1]=new Student(21,"李四",89.1);
        student[2]=new Student(22,"王五",73.9);
        System.out.println(Arrays.toString(student));//排序前
        Arrays.sort(student);//通过Arrays.sort对整个数组进行排序
        System.out.println(Arrays.toString(student));//排序后
    }

    public static void main2(String[] args) {
        Student s1=new Student(20,"张三",86.7);
        Student s2=new Student(21,"李四",89.1);
        //我们是不能直接比较s1和s2的
        System.out.println(s1.compareTo(s2));//s1.score>s2.score 返回值1,等于返回值0，小于返回值-1
    }
}
