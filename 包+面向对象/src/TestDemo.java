import com.bit.demo1.Demo1;

import java.util.Arrays;
import java.util.Date;
//import static java.lang.System.*;
/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-11-14
 * Time: 13:53
 */

interface Ia{
    void funcA();//默认为public abstract
}
interface Ib{
    void funcB();
}
class c implements Ia,Ib{
    @Override
    public void funcA() {

    }

    @Override
    public void funcB() {

    }
}












interface IShape{//接口用interface修饰
    //接口的所有方法都用public
    //抽象方法默认public abstract
    public abstract void draw();//抽象方法
    default public void func(){
        //接口中普通方法不能有具体实现，如果一定要实现，在方法前加default
        //default是默认的意思，也就是该方法是接口默认方法
        System.out.println("第一个默认方法");
    }
    default public void func2(){//接口可以有n多个默认方法
        System.out.println("第二个默认方法");
    }
    public static void staticFunc(){//接口可以有多个静态方法
        System.out.println("静态方法");
    }
    public static final int a=1;//接口中也可以包含字段，但必须是静态常量（final static），这里必须定义a后必须初始化
    //这里你直接写public int a=1;也可，因为是默认static final的
}
class Triangle implements IShape{//类和接口之间的关系是通过implements实现的
    //一个类实现了一个接口必须重写抽象方法，
    // 默认方法/静态方法你愿意重写就重写，不愿意也可以不写
    @Override
    public void draw() {
        System.out.println("画一个三角");
    }
    @Override//默认方法可重写，可不重写
    public void func(){
        System.out.println("重写接口中默认方法");
    }
}


abstract class shape{
    public int a;
    public void func(){
        System.out.println("一个普通方法");
    }
    public abstract void draw();
    /*{
        //System.out.println("正在画画");
    }*/
}
abstract class A extends shape{
    public abstract void func2();
}
class B extends A{
    @Override
    public void draw() {

    }
    @Override
    public void func2() {

    }
}
class Rect extends shape{
    @Override//子类如果继承抽象类，必须重写抽象类所有的抽象方法
    public void draw() {
        System.out.println("正在画方块");
        System.out.println(a);//子类继承抽象类可以访问抽象类里面的成员
        super.func();//也可以通过super来调用抽象类里面的普通方法
    }
}
class Cycle extends shape{
    @Override
    public void draw(){//重写快捷按键：dr
        System.out.println("正在画圆");
    }
}
class Flower extends shape{
    @Override
    public void draw(){
        System.out.println("正在画花");
    }
}

public class TestDemo {

    public static void main(String[] args) {

    }

    /*public static void main(String[] args) {
        IShape iShape=new Triangle();
        iShape.draw();
        iShape.func();
    }*/
    /*public static void drawMap(shape shape) {
        shape.draw();//多态
    }
    public static void main(String[] args) {
        //抽象类也可以向上转型、动态绑定
        shape s=new Rect();//向上转型（直接赋值）
        drawMap(s);
        Cycle cycle=new Cycle();
        drawMap(cycle);//向上转型（方法传参）
    }*/



    /*public static void main(String[] args) {
        *//*Flower flower=new Flower();
        flower.draw();*//*
        shape s=new shape();
        s.draw();
        shape s1=new Flower();
        s1.draw();
        shape s2=new Cycle();
        s2.draw();
    }*/



    /*public static void main(String[] args) {
        Demo1 d=new Demo1();
        //System.out.println(d.val);//到其他包里面会报错
    }*/




    public static void main0(String[] args) {
        int[]array={1,2,3,4,5};
        System.out.println(Arrays.toString(array));
        //注：Arrays.toString( )是通过类名调用的方法——一定是静态方法
        Date data=new Date();
        //如果你不想导入java.util这个包中的Date类，你可以按如下写法实现相同的效果
        java.util.Date date=new java.util.Date();//效果和上面的一样，但写法比较麻烦
        System.out.println(data.getTime());
    }

    /*public static void main1(String[] args) {
        out.println("hello");
    }*/


}
