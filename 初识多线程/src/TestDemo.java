/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-12-10
 * Time: 13:48
 */

//多任务：比如你边吃饭，边玩手机

//多线程：比如原先一条窄路，一次只能通行1辆车，但是后面开辟了多条道路，就可以同时同行多两车
//多线程放在程序里面就是，main函数和其他函数同时运行，而不是一个时间只能运行一个函数

//多进程：比如你边打游戏边听音乐，打游戏那边是一个进程，听音乐也是一个进程
    //同一个进程里面可以有多个线程（至少有一个线程），比如你看视频，会有音乐的线程，图像的线程，还有弹幕的线程...
    //ps:不同线程的优先权可能不一样，优先权高的会先执行

//进程：process
//线程：Thread


public class TestDemo  {
    //创建一个新的线程的两种方法

    //法1：将一个类声明为Thread的子类，这个子类需要重写run类的方法，创建线程对象，调用start()方法启动线程
    /*public static class xc extends Thread{

        public void run(){
            for (int i = 0; i < 10; i++) {
                System.out.println("线程测试"+i);
            }
        }
    }

    public static void main(String[] args) {
        //main函数是主线程

        //创建一个线程对象
        xc x=new xc();

        //用start方法开启线程
        x.start();

        for (int i = 0; i < 100; i++) {
            System.out.println("正在学习线程"+i);
        }//两条执行路径——主线程和子线程“并行交替执行”
    }*/


    //法2：创建一个类声明实现类Runnable的接口，我们创建的类要重写run方法，执行线程需要丢入runnable接口实现类
    /*public static class xc implements Runnable{

        public void run(){
            for (int i = 0; i < 10; i++) {
                System.out.println("线程测试"+i);
            }
        }
    }

    public static void main(String[] args) {
        //main函数是主线程

        //创建一个runnable接口的实现对象
        xc x=new xc();


        //创建线程对象，通过线程对象来开始我们的线程
        *//*Thread thread=new Thread(x);
        thread.start();*//*

        //上面两行代码的简便写法
        new Thread(x).start();


        for (int i = 0; i < 100; i++) {
            System.out.println("正在学习线程"+i);
        }//两条执行路径——主线程和子线程“并行交替执行”
    }*/

    //小结：
    //法一：继承Thread类，不建议使用，避免了OOP单继承局限性
    //法二：实现Runnable接口，建议使用，避免单继承局限性，灵活方便，可以同一个对象被多个线程使用


    //关于同一个对象被多个线程使用是这样的
    //比如买火车票
    private int ticketNum=10;
    public void run(){
        while(true){
            System.out.println(Thread.currentThread().getName()+"抢到了第"+ticketNum+"张火车票");
        }
    }
    public static void main(String[] args) {

    }

}
