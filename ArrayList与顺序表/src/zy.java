import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-01-05
 * Time: 19:27
 */
public class zy {
    //输入：每组数据输入一个字符串，字符串最大长度为100，且只包含字母，不可能为空串，区分大小写。
    //输出：每组数据一行，按字符串原有的字符顺序，输出字符集合，即重复出现并靠后的字母不输出

    //法1：用库方法contains，对每个字符判断一下
    /*public static void fun(String str) {
        StringBuilder stringBuilder=new StringBuilder();
        for(int i=0;i<str.length();i++){
            char ch=str.charAt(i);
            if(!stringBuilder.toString().contains(ch+"")){
                //stringBuilder没有contains，但是toString有
                //ch是一个字符，contains要的参数是字符串，加一个“”即可变为字符串
                stringBuilder.append(ch);//不包含就放进去
            }
        }
        System.out.println(stringBuilder);
        //或者你这里设置返回值类型String，return stringBuilder.toString()
    }
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while(scanner.hasNextLine()){//多组用例输入
            String str=scanner.nextLine();
            fun(str);
        }
    }*/


    //法二：利用哈希的思想
    //由题知只输入大小写字母，a~z对应ASCII码为：97~122，A~Z对应ASCII码为65~90
    //创建一个整形数组int[]，该数组用于存放每个字母存放的次数，数组对应下标为该字母的ASCII码
    //比如abcqweracb中的a
    //a对应的ASCII码为97，我们在数组下标97的位置放一个零，
    // 当第一次遇见有字符a的时候，0变成1，后续再有a，发现数组里存放的数是1，便不再添加
    /*public static String fun2(String str) {
        StringBuilder stringBuilder=new StringBuilder();
        int[]arr=new int[127];
        //ASCII码所有字符，你要是觉得大，你限制到122也可以，反正题目只输入大小写字母
        for(int i=0;i<str.length();i++){
            char ch=str.charAt(i);
            if(arr[ch]==0){
                stringBuilder.append(ch);
                arr[ch]=1;
            }
        }
        return stringBuilder.toString();
    }

    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while(scanner.hasNextLine()){//多组用例输入
            String str=scanner.nextLine();
            String str2=fun2(str);
            System.out.println(str2);
        }
    }*/




    //删除第一个字符串当中出现的第二个字符串的字符
    //例如：
    //String str1=“welcome to bit”;
    //String str2="come";
    //输出结果wl t bit

    //思路：用一个i 遍历str1，如果str1中的字符在str2中不存在，放到一个ArrayList里面
    /*public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入str1");
        String str1=scanner.nextLine();
        System.out.println("请输入str2");
        String str2=scanner.nextLine();
        ArrayList<String> arrayList=new ArrayList<>();
        //我这里<>里放String,你放Character（字符）也可以
        for(int i=0;i<str1.length();i++){
            char ch=str1.charAt(i);//拿到字符串str1下标为i的字符
            if(!str2.contains(ch+"")){
                arrayList.add(ch+"");
                //因为我之前是泛型是String,所以只能添加字符串类型进去，这里字符转字符串要加""
                //如果你之前泛型用Character直接add里面放ch即可
            }
        }
        System.out.println(arrayList);
    }*/

    /*public static void main(String[] args) {
        ArrayList<Integer> integers=new ArrayList<>();
        integers.add(1);
        integers.add(3);
        integers.add(5);
        integers.add(51);
        integers.add(6);
        integers.add(2);
        integers.add(21);
        Collections.sort(integers);//通过Collection.sort()对ArrayList排序
        System.out.println(integers);
    }*/



    //斐波那契数列：
    public static void generate(int numRows){
        List<List<Integer>> ret=new ArrayList<>();

        //第一行：
        List<Integer> list1=new ArrayList<>();
        list1.add(1);
        ret.add(list1);//把第一行数据放到ret里面
        //后续n-1行
        for(int i=1;i<numRows;i++){
            List<Integer> list=new ArrayList<>();
            list.add(1);//每行第一个数据都是1
            //中间数据
            List<Integer> preRow= ret.get(i-1);//获取上一行下标为i-1的数
            for(int j=1;j<i;j++){
                int num= preRow.get(j)+preRow.get(j-1);
                list.add(num);
            }
            list.add(1);//每行最后一个数据都是1
            ret.add(list);
        }
        for(int i=0;i<ret.size();i++){//ret里面存了size个一维数组
            for(int j=0;j<ret.get(i).size();j++){//每个一维数组里存了每一行的数列
                System.out.print(ret.get(i).get(j)+" ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        generate(n);
    }
}
