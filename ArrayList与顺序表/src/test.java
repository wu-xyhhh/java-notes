import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-01-08
 * Time: 19:27
 */
//用ArrayList实现一副扑克牌
//没有大小王，数字1-13，10-13表示J,Q,K
//四个花色：黑桃♠，红桃♥，方块◇，梅花♣
class Card{//扑克牌
    private  int rank;//数字
    private  String suit;//花色

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getSuit() {
        return suit;
    }

    public void setSuit(String suit) {
        this.suit = suit;
    }

    public Card(int rank, String suit) {
        this.rank = rank;
        this.suit = suit;
    }

    @Override
    public String toString() {
        return "Card{" +
                "数字：" + rank +
                ", 花色='" + suit + '\'' +
                '}';
    }
}

public class test {
    public static final String[]suits={"♥","♠","◇","♣"};//四种花色
    private static List<Card> makeCard(){
        ArrayList<Card>cards=new ArrayList<>();//这里你不用ArrayList用List也可以
        for(int i=0;i<4;i++){//4中花色
            for(int j=0;j<=13;j++){//每个花色13张牌（1-9，JQK)
                String suit=suits[i];
                Card card=new Card(j,suit);
                cards.add(card);
                //上面3行代码可以优化为一行代码，如下
                //cards.add(new Card(j,suit[i]));
            }
        }
        return cards;
    }
    private static void swap(List<Card> cards,int i,int j){//交换 下标为i的牌和下标为j（随机生成数）的牌
        //类似于数组元素交换中的
        //Card tmp=cards[i];
        Card tmp= cards.get(i);

        //cards[i]=cards[j]
        cards.set(i, cards.get(j));

        //cards[j]=tmp;
        cards.set(j,tmp);
        //注意！！！上面三行注释是给你一个形象的比喻，我们是面向对象的交换，终究不是简单的数组元素交换

    }
    private static void shuffle(List<Card> cards){
        //直接通过makeCard()方法出来的扑克牌是有序的，
        //但现实里一般扑克牌都是乱序的，也就是常说的洗牌
        int size= cards.size();//共多少张牌
        for(int i=size-1;i>0;i--){//保证每张牌都洗过，
            // i=size-1是牌下标最多到size-1，
            // i>0是因为下面的random.next(i);i是生成0-（i-1)的数字，i-1>=0
            Random random=new Random();
            int rand=random.nextInt(i);//生成0-9的下标，传n进去，生成0-（n-1）的数字(下标)，共n个数
            swap(cards,i,rand);
        }


    }
    /*public static void main(String[] args) {
        List<Card>cards= makeCard();//来一副“有顺序”的牌
        System.out.println(cards);//打印“有顺序”的牌
        shuffle(cards);//洗牌
        System.out.println(cards);//打印“洗过牌”的牌
    }*/

    //模拟三个人打牌
    public static void main(String[] args) {
        List<Card>cards =makeCard();
        System.out.println("牌组创建完成:"+cards);
        shuffle(cards);
        System.out.println("洗牌已完成："+cards);
        System.out.println("下面进行揭牌：3人每人轮流揭5张牌");
        ArrayList<ArrayList<Card>> hand=new ArrayList<>();
        //类似二维数组
        //首先你有一个hand，hand里面的每一个元素都是ArrayList<Card>
        //ArrayList里面每个元素都是Card类型
        ArrayList<Card> hand1=new ArrayList<>();//第一个人手上的牌
        ArrayList<Card> hand2=new ArrayList<>();//第二个人手上的牌
        ArrayList<Card> hand3=new ArrayList<>();//第三个人手上的牌
        hand.add(hand1);
        hand.add(hand2);
        hand.add(hand3);

        //每个人，轮流发牌
        for(int i=0;i<5;i++){
            for(int j=0;j<3;j++){
                Card card= cards.remove(0);
                hand.get(j).add(card);
            }
        }
        System.out.println("第一个人的牌："+hand1);
        System.out.println("第一个人的牌："+hand2);
        System.out.println("第一个人的牌："+hand3);
        System.out.println("剩余牌："+hand);
    }
}
