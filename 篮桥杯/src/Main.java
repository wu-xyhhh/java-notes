

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-01-09
 * Time: 14:33
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    //杨辉三角
    //法一：递归思想
    /*public static int dg(int i,int j) {//Aij=Ai-1j-1 +Ai-1j ,A表示数字，ij表示下标
        if(j==0||j>i){
            return 0;
        }else{
            if(i==1||j==1){
                return 1;
            }else{
                return dg(i-1,j-1)+dg(i-1,j);
            }
        }
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int i = 0;
        int j=0;
        for ( i = 1; i <= n; i++) {//i表示第i行，共n行
            for ( j = 1; j <= i; j++) {//每行i个数
                System.out.print(dg(i, j)+" ");
            }
            System.out.println();
        }
    }*/



    //十一届例题：成绩分析
    /*public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        ArrayList<Integer> arrayList=new ArrayList<>();
        for(int i=0;i<n;i++){
            arrayList.add(scanner.nextInt());
        }
        int max=0,min=arrayList.get(0);
        int i=0,sum=0;
        for(i=0;i<arrayList.size();i++){
            if(max<=arrayList.get(i)){
                max=arrayList.get(i);
            }else if(min>arrayList.get(i)){
                min=arrayList.get(i);
            }
            sum+=arrayList.get(i);
        }
        System.out.println(max);//最高分
        System.out.println(min);//最低分
        //平均分，保留两位小数
        float f = (float) sum/(arrayList.size());
        System.out.printf("%.2f",f);
    }*/


    //找到小镇的法官-力扣997
//    public int findJudge(int n, int[][] trust) {
//        //sx1和sx2分别表示题目中的属性1和2
//        //sx1[i]表示第i个人 信任的人的个数
//        //sx2[i]表示第i个人 被其他人信任的个数
//        //我们只要遍历sx1数组找到sx1[i]==0,且sx2[i]==n-1即可
//
//        int []sx1=new int[n+1];
//        int []sx2=new int[n+1];
//        for(int[] temp:trust)
//        {
//            //temp里面一共2个元素，temp[0]信任temp[1]
//            sx1[temp[0]]++;
//            sx2[temp[1]]++;
//        }
//        int i=0;
//        for(i=1;i<=n;i++)
//        {
//            //in[i]==0表示法官不相信任何人
//            //out[i]==N-1表示其余人都相信法官
//            if(sx1[i]==0&&sx2[i]==n-1)
//            {
//                return i;
//            }
//        }
//        return -1;
//    }

    //二维网格迁移-力扣1260
//    public List<List<Integer>> shiftGrid(int[][] grid, int k) {
//        int h=grid.length;//行数
//        int l=grid[0].length;//列数
//        int i=0;
//        int j=0;
//        while(k>0){
//            int[][] grid2=new int[h][l];
//            for( i=0;i<h-1;i++){//i<h-1,后面有i+1的情况，如果这里i<h，i+1后会越界
//                for( j=0;j<l;j++){
//                    if(j<l-1){
//                        grid2[i][j+1]=grid[i][j];
//                    }
//                    if(j==l-1){
//                        grid2[i+1][0]=grid[i][j];
//                    }
//                }
//            }
//            //外面的for走完，i=h-1
//            for( j=0;j<l;j++){
//                if(j<l-1){
//                    grid2[h-1][j+1]=grid[i][j];
//                }
//                if(j==l-1){
//                    grid2[0][0]=grid[i][j];
//                    }
//                }
//
//            k--;
//            grid=grid2;
//        }
//        List<List<Integer>> result = new ArrayList<>();
//        for (int[] row : grid) {
//            List<Integer> listRow = new ArrayList<>();
//            result.add(listRow);
//            for (int v : row) listRow.add(v);
//        }
//        return result;
//    }


    //杨辉三角
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        ArrayList<Integer> arrayList=new ArrayList<>();

    }

}
