import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-01-01
 * Time: 14:57
 */
public class TestDemo {
    //Collection常用方法

    /*public static void main(String[] args) {
        Collection<String> collection=new ArrayList<String>();
        //Collection是一个接口，是不能直接创建对象的，你可以引用一个它的具体实现类
        collection.add("hello");//往collection里放一个字符串
        collection.add(1);//往collection里放一个整形
        //到这里你会发现，collection好像什么都可以放，放入字符串后还能放整形，也没有报错
        //但这种操作方式太宽泛了，你到时候要从collection里面取东西也不会确定类型
    }*/

        /*public static void main(String[] args) {
            Collection<Integer> collection=new ArrayList<>();
            collection.add(1);//往collection里放一个整形1
            collection.add(2);//往collection里放一个整形2
            System.out.println(collection);//打印[1,2]
            collection.clear();//清空collection里的元素
            System.out.println(collection);//打印[],[]里面没有元素
            System.out.println(collection.isEmpty());//打印true（collection里面没有元素）

        }*/

    /*public static void main(String[] args) {
        Collection<Integer> collection=new ArrayList<>();
        collection.add(1);//往collection里放一个整形1
        collection.add(2);//往collection里放一个整形2
        collection.add(3);//往collection里放一个整形3
        Object []objects=collection.toArray();//toArray方法，返回一个装有所以元素的数组，返回类型位Object
        System.out.println(Arrays.toString(objects));//打印objects这个数组里的元素
    }*/


    /*public static void main(String[] args) {
        Collection<Integer> collection=new ArrayList<>();
        collection.add(1);//往collection里放一个整形1
        collection.add(2);//往collection里放一个整形2
        System.out.println(collection);//打印[1,2]
        collection.remove(1);
        System.out.println(collection);//打印[2]
    }*/

    //Map常用方法：
    //put和get
    /*public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        //这里的Map和前面说的Collection一样是接口，
        // 你不能直接用Map来new对象，你可以引用一个它的具体实现类，我们这里用HashMap
        //这里的map key值和value值类型都是String
        map.put("我爱", "中国");//往map里面放元素，key值为“我爱”，value值为“中国”
        map.put("我喜欢吃","西瓜");
        System.out.println(map);//打印{我爱=中国, 我喜欢吃=西瓜}
    }*/


    /*public static void main(String[] args) {
     Map<String, String> map = new HashMap<>();
        //这里的Map和前面说的Collection一样是接口，
        // 你不能直接用Map来new对象，你可以引用一个它的具体实现类，我们这里用HashMap
        //这里的map key值和value值类型都是String
        map.put("我爱", "中国");//往map里面放元素，key值为“我爱”，value值为“中国”
        map.put("我喜欢吃","西瓜");
        String ret = map.get("我爱");//get方法：根据key值找value值
        System.out.println(ret);//打印 中国
        //如果我们给的key值和预期的不一样会发生什么
        String ret2=map.get("我爱你");
        System.out.println(ret2);//打印null

        //getOrDefault
        String ret3=map.getOrDefault("我爱你","hhh");
        System.out.println(ret3);//打印hhh
        //和get方法的区别就是，getOrDefault找不到的时候会用后面的参数，而不是null
        }*/

    /*public static void main(String[] args) {
        //containsKey
        Map<String, String> map = new HashMap<>();
        //这里的Map和前面说的Collection一样是接口，
        // 你不能直接用Map来new对象，你可以引用一个它的具体实现类，我们这里用HashMap
        //这里的map key值和value值类型都是String
        map.put("我爱", "中国");//往map里面放元素，key值为“我爱”，value值为“中国”
        map.put("我喜欢吃","西瓜");
        boolean flg1=map.containsKey("我爱");//判断map中是否包含某个key值
        System.out.println(flg1);//打印true
        boolean flg2=map.containsKey("我爱你");
        System.out.println(flg2);//打印false

        //containsValue
        boolean flg3=map.containsValue("中国");//判断map中是否包含某个value值
        System.out.println(flg3);//打印true
        boolean flg4=map.containsValue("吃饭");
        System.out.println(flg4);//打印false
    }*/

    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        //这里的Map和前面说的Collection一样是接口，
        // 你不能直接用Map来new对象，你可以引用一个它的具体实现类，我们这里用HashMap
        //这里的map key值和value值类型都是String
        map.put("我爱", "中国");//往map里面放元素，key值为“我爱”，value值为“中国”
        map.put("我喜欢吃","西瓜");
        Set<Map.Entry<String,String>> entrySet= map.entrySet();//我们map的key和value都是String，把对应位置换成String即可
        //调用Set<Map.Entry<K, V>> entrySet()的时候，会将你的key和对应的value组装起来
        //组装起来后，会放入entrySet中
        for(Map.Entry<String,String> entry:entrySet){
            //for each（），左边参数用来接受右边参数的内容
            //Map.Entry<String,String>是左边参数的类型
            System.out.println("key:"+entry.getKey()+"value:"+entry.getValue());//打印key和value
        }
    }
}
