/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-12-05
 * Time: 9:19
 */
public class TestDemo {
    /*public static void main(String[] args) {
        String str="123";
        int ret=Integer.valueOf(str);//将字符串变成整数
        System.out.println(ret);//打印123
        //没有包装类，我们调用不了valueOf（）方法，
        // 你需要自己写一个方法来将字符串转换为整数

        //这也就是包装类的好处所在——包装类可以帮助我们对数据进行一些处理，换句话说，我们可以将简单数据也变成对象
        //也印证了那句话——java一切皆对象

        //ps：String不是包装类，它就是一个普通的类
    }*/


    //装箱——把简单类型 变成 包装类类型
    //拆箱——把包装类类型 变成 简单类型
    /*public static void main(String[] args) {
        Integer a=123;//（隐式的）装箱——底层会调用Integer.valueOf
        int b=a;//（隐式的）拆箱——底层会调用Integer.intValue
        System.out.println(a+" "+b);//打印123 123

        Integer c=Integer.valueOf(123);//（显式）的装箱
        Integer d=new Integer(123);//Integer是一个类，你可以通过它来new一个对象
        int e=c.intValue();//显式的拆包
        double f=c.doubleValue();//这里也可以通过c这个包装类类型变成double型（简单类型都可以）
        System.out.println(e);//打印123
        System.out.println(f);//打印123.0
    }*/


    public static void main(String[] args) {
        Integer a=128;
        Integer b=128;
        System.out.println(a==b);
    }
}
