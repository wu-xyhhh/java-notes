//加上一个逻辑：通过GET/login这个接口来获取当下的登录状态
function getUserInfo(){
    $.ajax({
        type:'get',
        url:'login',
        success: function(body){
            //判断此处的body是不是有效的user对象(userId是否非0)
            if(body.userId&&body.userId>0){
                //登录成功，不做处理
                console.log(body.username+"登录成功");

                //把根据当前用户登录的情况，把当前用户名设置到页面上
                changeUserName(body.username);
            }else{
                //登录失败，前端页面跳转到login.html
                alert("当前尚未登录，请登录后再访问博客列表");
                location.assign('blog_login.html');
            }
        },
        error:function(){
            alert("当前尚未登录，请登录后再访问博客列表");
            location.assign('blog_login.html');
        }
    });
}
getUserInfo();

function changeUserName(username){

    let h3=document.querySelector('.card>h3');
    h3.innerHTML=username;
}