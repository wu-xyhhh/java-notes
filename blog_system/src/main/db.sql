--编写建库建表的sql

create database if not exists wxy_blog;
use wxy_blog;

--创建一个博客表
drop table if exists blog;
create table blog(
   blogId int primary key auto_increment,
   title varchar(1024),
   content mediumtext,
   userId int,
   postTime datetime
);
--给博客表插入一些数据，进行测试
insert into blog values(null,'this is my first blog','i love java'，1，now());
insert into blog values(null,'这是第二篇博客','从今天开始，我要认真学js'，1，now());
insert into blog values(null,'这是第一篇博客','从今天开始，我要认真学c'，1，now());
insert into blog values(null,'这是第一篇博客','从今天开始，我要认真学c++'，2，now());
insert into blog values(null,'这是第二篇博客','c++真nm好玩'，2，now());
insert into blog values(null,'这是第三篇博客','# 我是一级标题\n ### 我是二级标题\n >这是引用内容',2,now());
--创建一个用户表
create table user(
   userId int primary key auto_increment,
   username varchar(128) unique ,
   password varchar(128)
);

insert into user values(null,'wxy','123');
insert into user values(null,'list','123');
