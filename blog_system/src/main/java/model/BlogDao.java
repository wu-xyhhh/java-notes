package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-07-03
 * Time: 22:20
 */
public class BlogDao {
//    封装博客表的基本操作（增删查）
    //1.往博客表里插入一个博客
    public void insert(Blog blog){
        //jdbc基本代码
        Connection connection=null;
        PreparedStatement statement=null;

        //1.和数据库建立连接
        try {
            connection= DBUtil.getConnection();
            //2.构造sql语句
            String sql="insert into blog values(null,?,?,?,now())";
            statement=connection.prepareStatement(sql);
            statement.setString(1,blog.getTitle());
            statement.setString(2, blog.getContent());
            statement.setInt(3,blog.getUserId());
            //3.执行sql
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            //4.关闭连接，释放资源
            DBUtil.close((com.mysql.jdbc.Connection) connection,statement,null);
        }

    }

    //2.能获得博客表中所有的博客信息（用于博客列表页，不一定是完整正文）
    public List<Blog> selectAll(){
        List<Blog> blogs=new ArrayList<>();
        Connection connection=null;
        PreparedStatement statement=null;
        ResultSet resultSet=null;
        try {
            connection= DBUtil.getConnection();
            String sql="select * from blog order by postTime desc";
            statement=connection.prepareStatement(sql);
            resultSet=statement.executeQuery();
            while(resultSet.next()){
                Blog blog=new Blog();
                blog.setBlogId(resultSet.getInt("blogId"));
                blog.setTitle(resultSet.getString("title"));
                //需要针对内容进行截断，如果太长了就去掉后面
                String content=resultSet.getString("content");
                if(content.length()>50){
                    content=content.substring(0,50)+"...";
                }
                blog.setContent(content);
                blog.setUserId(resultSet.getShort("userId"));
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                blogs.add(blog);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close((com.mysql.jdbc.Connection) connection,statement,resultSet);
        }
        return blogs;
    }

    //3.能够根据博客id获取到指定的博客内容（用于博客详情页）
    public static Blog selectOne(int blogId){
        Connection connection=null;
        PreparedStatement statement =null;
        ResultSet resultSet=null;
        try {
            connection= DBUtil.getConnection();
            String sql="select * from blog where blogId=?";
            statement=connection.prepareStatement(sql);
            statement.setInt(1,blogId);
            resultSet=statement.executeQuery();
            //按id查，要么查到1条要么是0条，这里就不用while了，直接用if即可
            if(resultSet.next()){
                Blog blog=new Blog();
                blog.setBlogId(resultSet.getInt("blogId"));
                blog.setTitle(resultSet.getString("title"));
                blog.setContent(resultSet.getString("content"));
                blog.setUserId(resultSet.getShort("userId"));
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                return blog;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close((com.mysql.jdbc.Connection) connection,statement,resultSet);
        }
        return null;
    }
    //4.根据博客id删除博客
    public void delete(int blogId){
        Connection connection=null;
        PreparedStatement statement =null;
        try {
            connection= DBUtil.getConnection();
            String sql="delete from blog where blogId=?";
            statement=connection.prepareStatement(sql);
            statement.setInt(1,blogId);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close((com.mysql.jdbc.Connection)connection,statement,null);
        }
    }

}

