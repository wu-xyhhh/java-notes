package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.User;
import model.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-07-05
 * Time: 17:00
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private ObjectMapper objectMapper=new ObjectMapper();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf8");//针对请求，表示用utf8格式来解析请求
        resp.setCharacterEncoding("utf8");//针对响应，表示构造的数据要安装utf8构造
        //1，获取请求中的参数
        String username=req.getParameter("username");
        String password=req.getParameter("password");
        if(username==null || "".equals(username)||password==null||"".equals(password)){
            //请求内容缺失
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("当前的用户名或密码为空，请重新输入");
            return;
        }
        //2.和数据库中的内容进行比较
        UserDao userDao=new UserDao();
        User user=userDao.selectByName(username);
        if(user==null||!user.getPassword().equals(password)){
            //用户名不存在或者密码错误
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("当前的用户名或密码错误，请重新输入");
            return;
        }
        //3.如果比较通过，就创建会话
        HttpSession session= req.getSession(true);
        //把刚才的用户信息存储到会话中
        session.setAttribute("user",user);

        //4.返回一个重定向报文，跳转到博客列表页
        resp.sendRedirect("blog_list.html");
    }

    //这个方法用来让前端检查当前登录状态
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=utf8");
        HttpSession session=req.getSession(false);
        if(session==null){
            //检查一下会话是否存在，如果不存在说明未登录
            User user=new User();
            resp.getWriter().write(objectMapper.writeValueAsString(user));
            return;
        }
        User user=(User) session.getAttribute("user");
        if(user==null){
            //虽然有会话,但是会话里没有user对象，但是也视为未登录
            user=new User();
            resp.getWriter().write(objectMapper.writeValueAsString(user));
            return;
        }
        //已经登录的状态
        user.setPassword("");//此处不能把密码返回到前端
        resp.getWriter().write(objectMapper.writeValueAsString(user));
    }
}
