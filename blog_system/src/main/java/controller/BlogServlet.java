package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Blog;
import model.BlogDao;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-07-04
 * Time: 15:05
 */
//通过这个类来处理当前博客列表页的相关方法
@WebServlet("/blog")
public class BlogServlet extends HttpServlet {
    private ObjectMapper objectMapper=new ObjectMapper();
    //这个方法用来获取到数据库中的博客列表
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BlogDao blogDao=new BlogDao();
        resp.setContentType("application/json; charset=utf8");
        //先尝试获取到req中的blogId参数，如果该参数存在，说明要请求的博客详情
        //如果该参数不存在，说明是要请求博客的列表

        String param= req.getParameter("blogId");
        if(param==null){
            //不存在参数，获取博客列表
            //从数据库中查询到博客列表，转成json格式，然后直接返回即可

            List<Blog> blogs= blogDao.selectAll();
            //然后把blogs对象转成json格式
            String respJson=objectMapper.writeValueAsString(blogs);

            resp.getWriter().write(respJson);
            //构造响应的时候，上面两行代码不能颠倒
            //如果先write了body，再设置ContentType，设置的ContentType就无法生效
        }else{
            //存在参数，获取博客详情页
            int blogId=Integer.parseInt(param);
            Blog blog=BlogDao.selectOne(blogId);
            String respJson= objectMapper.writeValueAsString(blog);
            resp.getWriter().write(respJson);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session=req.getSession(false);
        if(session==null){
            //用户未登录,不能提交博客
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前用户未登录！");
            return;
        }
        User user=(User) session.getAttribute("user");
        if(user==null){
            //用户未登录,不能提交博客
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前用户未登录！");
            return;
        }

        //一定要先指定好请求按照哪种编码来解析
        req.setCharacterEncoding("utf8");

        //先从请求中，取出博客的标题和正文
        String title=req.getParameter("title");
        String content=req.getParameter("content");
        if(title==null||"".equals(title)||content==null||"".equals(content)){
            //直接告诉客户端，请求参数不对
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("请求博客失败！缺失必要参数");
            return;
        }
        //构造blog对象，把当前的信息填进去，并插入数据库中
        Blog blog=new Blog();
        blog.setContent(title);
        blog.setContent(content);
        //作者id是当前提交这个博客的身份信息
        blog.setUserId(user.getUserId());
        BlogDao blogDao=new BlogDao();
        blogDao.insert(blog);
        //重定向到博客列表页
        resp.sendRedirect("blog_list.html");
    }
}
