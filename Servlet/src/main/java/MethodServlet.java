import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-07-02
 * Time: 10:44
 */
@WebServlet("/method")//请求以/method结束，执行下面的类
//同一个webapp中，多个Servlet关联的路径不能相同
public class MethodServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        resp.setContentType("test/html; charset=utf8");
        resp.getWriter().write("post 响应");
        //想要构造post请求，一般两种方法
        //1.form表单
        //2.ajax
    }
}
