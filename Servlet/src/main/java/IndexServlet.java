import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-07-08
 * Time: 15:23
 */
@WebServlet("/index")
public class IndexServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //返回一个主页（简单的html片段）
        //此处需要得到用户名（从HttpSession片段中可得）
        HttpSession session=req.getSession(false);
        //此处getSession的参数必须是false，前面在登录过程中，已经创建过会话了，此处是要直接获取之前的会话
        String username=(String) session.getAttribute("username");

        //除了用户名外，还从会话中取出count
        Integer count=(Integer)session.getAttribute("count");
        count+=1;
        //把自增后的值再写回到会话中
        session.setAttribute("count",count);

        resp.setContentType("text/html;charset=utf8");
        resp.getWriter().write("<h3>欢迎你"+username+"这是第"+count+"次访问"+"<h3>");
    }
}
