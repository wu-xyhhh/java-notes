import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-07-08
 * Time: 15:00
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //处理用户请求
        String username=req.getParameter("username");
        String password=req.getParameter("password");
        //判断用户名或密码是否正确
        //一般来说这个判断操作需要放到数据库存取的
        //我们这里为了简单，就直接在代码里写死了，假设有效用户名和密码是“zhangsan”，“123”
        if("zhangsan".equals(username)&&"123".equals(password)){
            //为什么不写username.equals("zhangsan")?
            //一旦用户不输入用户名直接点登录，那就是null.equals,会出现空指针异常

            //登录成功
            //创建会话，并保存必要的身份信息
            //会话里可以保存任意指定的用户信息，比如我们这里可以实现一个简单的功能，记录用户访问主页的次数
            HttpSession httpSession= req.getSession(true);

            //往会话中存储键值对（必要信息）
            httpSession.setAttribute("username",username);

            //初始情况下，把登录次数count设为0
            httpSession.setAttribute("count",0);
            //setAttribute的第二个参数是Object，int不是Object但是Integer是
            //在上面的httpSession.setAttribute("count",0);代码中会触发Java中的自动装箱机制，自动把0转成Integer
            //我们存的是Integer，所以在后续的IndexServlet中也是用Integer来取

            resp.sendRedirect("index");//跳转到index页面
        }else{
            //登录失败
            resp.getWriter().write("login failed");
        }
    }
}
