/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-06-03
 * Time: 17:32
 */
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/hello")
//把当前这个HelloServlet这个类，和HTTP请求中的URL里面，路径带有/hello这样的请求给关联起来了
public class helloServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //super.doGet(req, resp);这个super一定要注释掉，不能调用父类的doGet



        System.out.println("hello world");//这个是在idea里面打印

        //如果想在页面上打印hello world，就把hello world字符串放到http响应的body中
        //浏览器就会把body的内容显示到页面上
        resp.getWriter().write("hello world"+System.currentTimeMillis());
        //resp是响应对象，getWriter其实是返回了一个Writer对象（字符流对象）
        //此处的Writer对象就不是往文件里面写了，而是往http响应的body中写入数据
        //write是真正干活，写数据的方法
    }
}
