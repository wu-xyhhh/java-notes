import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import com.fasterxml.jackson.databind.ObjectMapper;
/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-07-06
 * Time: 16:06
 */

class User{
    public  int userId;
    public  int classId;
    //当前这两个属性都设置成public，
    //如果设为private，同时提供getter,setter，效果是一样的
}
@WebServlet("/postJson")
public class PostJsonServlet extends HttpServlet {
    //1.创建一个jackson核心对象
    private ObjectMapper objectMapper=new ObjectMapper();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       //2.读取body中的请求，然后使用ObjectMapper来解析成需要的对象
        //readValue就是把JSON格式的字符串，转成Java的对象

        User user=objectMapper.readValue(req.getInputStream(),User.class);
        //第一个参数，表示对哪个字符串进行转换，这个参数可以填成一个String，也可以填一个InputStream对象，还可以填一个File对象
        //第二个参数，表示要把这个JSON格式的字符串，转成哪个java对象
        //readValue是怎么完成转换的？
        //1.先把getInputStream对应的流对象里面的数据都读取出来

        //2.针对这个json字符串进行解析，从字符串转换成键值对，
        //比如key:userId;value:123  key:classId;value:456

        //3.遍历这个键值对，依次获取到每一个key，根据这个key名字，和User类里面的属性名字对比一下
        //如果发现匹配的属性，就把当前的key对应的value赋值到该User属性中（赋值过程中会同时进行类型转换）

        //4.当把所有的键值对都遍历结束，User对象也就被构造的差不多了


        resp.getWriter().write("userId: "+user.userId+",classId: "+user.classId);
    }
}
