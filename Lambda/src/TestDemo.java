import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-02-28
 * Time: 18:21
 */


//函数式接口
interface NoParameterNoReturn {
    //注意：只能有一个方法
    void test();
}

//无返回值无参数
@FunctionalInterface
interface NoParameterNoReturn0 {
    void test();
}
//无返回值一个参数
@FunctionalInterface
interface OneParameterNoReturn {
    void test(int a);
}
//无返回值多个参数
@FunctionalInterface
interface MoreParameterNoReturn {
    void test(int a,int b);
}
//有返回值无参数
@FunctionalInterface
interface NoParameterReturn {
    int test();
}

//有返回值一个参数
@FunctionalInterface
interface OneParameterReturn {
    int test(int a);
}
//有返回值多参数
@FunctionalInterface
interface MoreParameterReturn {
    int test(int a,int b);
}

public class TestDemo {
    public static void main0(String[] args) {
        NoParameterNoReturn parameterNoReturn=new NoParameterNoReturn(){
            @Override
            public void test(){
                System.out.println("这里是重写后的方法");
            }
        };
        parameterNoReturn.test();
    }

    public static void main1(String[] args) {
        //无返回值无参数
        NoParameterNoReturn parameterNoReturn=()-> System.out.println("这里是重写后的方法");
        parameterNoReturn.test();

        //无返回值一个参数
        //写法1
        OneParameterNoReturn oneParameterNoReturn=(a)->{System.out.println(a);};
        oneParameterNoReturn.test(1);//打印1
        //写法2
        OneParameterNoReturn oneParameterNoReturn1=a->System.out.println(a);
        //只有一个参数，小括号可以省略；只有一条语句，花括号可以省略
        oneParameterNoReturn1.test(1);//打印1

        //无返回值多个参数
        MoreParameterNoReturn moreParameterNoReturn=(a,b)->{System.out.println(a+b);};
        //这里语句也是一条，如果你想，也可以把花括号去掉，原理同上
        moreParameterNoReturn.test(1,2);//打印3


        //有返回值无参数
        NoParameterReturn noParameterReturn=()->{return 520;};//ps:return不写也是效果一样的
        int ret=noParameterReturn.test();
        System.out.println(ret);//打印520

        //有返回值一个参数
        OneParameterReturn oneParameterReturn=(a)->{return a+1;};
        //花括号内只要是一个返回式子就行，你甚至可以不带上a（不用传过来的参数）
        int ret1=oneParameterReturn.test(9);
        System.out.println(ret1);//打印10

        //有返回值多参数
        MoreParameterReturn moreParameterReturn=(a,b)->a*b;//相当于(a+b)->{return a*b;}
        int ret2=moreParameterReturn.test(3,8);
        System.out.println(ret2);//打印24
    }

    public static void main2(String[] args) {
        int a=10;
        new NoParameterNoReturn(){
            @Override
            public void test(){
                System.out.println("捕获变量："+a);
            }
        }.test();//正常打印——捕获变量：10
    }

    public static void main3(String[] args) {
        int x=1;
        OneParameterNoReturn oneParameterNoReturn=(a)->{
            System.out.println(a);
            System.out.println(x);};
        oneParameterNoReturn.test(2);//打印2 1
    }

    public static void main4(String[] args) {
        ArrayList<String>list=new ArrayList<>();
        list.add("a");
        list.add("bc");
        list.add("def");
        list.add("hello");

        //写法1：（不用Lambda表达式）
        list.forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        });

        //写法2：（用Lambda表达式）
        list.forEach(s-> System.out.println(s));
        //效果和写法1一样
    }


    public static void main5(String[] args) {
        ArrayList<String>list=new ArrayList<>();
        list.add("hh");
        list.add("hi");
        list.add("def");
        list.add("abc");

        //写法1：（不用Lambda表达式）
        list.sort(new Comparator<String>() {
            @Override
            public int compare(String o1,String o2) {
                return o1.compareTo(o2);
            }
        });
        list.forEach(s-> System.out.println(s));

        System.out.println("======分割线======");

        //写法2：（用Lambda表达式）
        list.sort(((o1, o2) -> o1.compareTo(o2)));
        //效果和写法1一样
        list.forEach(s-> System.out.println(s));
    }

    public static void main(String[] args) {
        HashMap<Integer,String>map=new HashMap<>();
        map.put(1,"hello");
        map.put(2,"I");
        map.put(3,"love");
        map.put(4,"china");

        //法一：（不用Lambda）
        map.forEach(new BiConsumer<Integer, String>() {
            @Override
            public void accept(Integer integer, String s) {
                System.out.println("key:"+integer+"value:"+s);
            }
        });

        System.out.println("======分割线======");

        //法二：（用Lambda）
        map.forEach((key,value)-> System.out.println("key:"+key+"value:"+value));
    }
}




