/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-01-20
 * Time: 15:16
 */
//编一个程序，读入用户输入的一串先序遍历字符串，
// 根据此字符串建立一个二叉树（以指针方式存储）。
// 例如如下的先序遍历字符串：
// ABC##DE#G##F### 其中“#”表示的是空格，空格字符代表空树。
// 建立起此二叉树以后，再对二叉树进行中序遍历，输出遍历结果。

//示例：
//输入：abc##de#g##f###
//输出：c b e g d f a
import java.util.*;
class TreeNode{//节点类
    public char val;
    public TreeNode left;
    public TreeNode right;
    public TreeNode(char val){
        this.val=val;
    }
}
public class Main {
    public static int i=0;
    public static TreeNode creatTree(String str){
        TreeNode root =null;
        if(str.charAt(i)!='#'){//判断下标i的字符是不是#
            root=new TreeNode(str.charAt(i));
            i++;
            root.left=creatTree(str);
            root.right=creatTree(str);
        }else{
            //遇到#,也就是空树，我们i++跳过即可
            i++;
        }
        return root;
    }
    public static void inorder(TreeNode root){//前序遍历-左根右
        if(root==null){
            return ;
        }
        inorder(root.left);
        System.out.print(root.val+" ");
        inorder(root.right);
    }
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while(scanner.hasNextLine()){
            String str=scanner.nextLine();
            TreeNode root=creatTree(str);
            inorder(root);
        }
    }
}


