import java.util.Arrays;
import java.util.Scanner;

public class test {
    public static void main0(String[] args) {
        //法一：传统定义数组并初始化
        int[] arr={1,2,3};//int[]是一种类型，[]里不能加东西，否则会报错
        //这里没有写new，但实际也是一个对象

        //法二：用new，定义数组并未初始化
        int[] arr2=new int[3];//new是java的关键字，用来实例化java的对象，java的数组也是一个对象
        //对象，你可以想象成一个具体的实物
        //这句代码效果相当于int[] arr2={0,0,0}

        //法三：用new，定义数组并初始化
        int[] arr3=new int[]{1,2,3};//这里new int[],[]里也是不能加东西的
        //法三这个情况下，编译器也会认为，给了一个长度为3数组，里面存放1,2,3，效果和法一一样
    }

    public static void main1(String[] args) {
        int[] arr={1,2,3,4,5,6,7};
        System.out.println(arr.length);
        System.out.println(arr[3]);
    }

    public static void main2(String[] args) {
        int[]arr={1,2,3,4,5,6};
        for(int i=0;i<arr.length;i++)
        {
            System.out.print(arr[i]+" ");
        }
    }

    //增强for循环
    public static void main3(String[] args) {
        int[]arr={1,2,3,4,5,6};
        for(int x:arr){//for里面定义一个整形x来接收整形数组里的每一个数
            System.out.print(x+" ");
        }
    }

    //通过java操作数组的工具类Arrays打印
    //用Arrays前需要import
    public static void main4(String[] args) {
        int []arr={1,2,3,4,5,6,7};
        String ret= Arrays.toString(arr);
        //Arrays.toString(arr)是把数组变成一个字符串，我们用字符串类型的ret来接收
        System.out.println(ret);
    }

    public static void main5(String[] args) {
        int []arr=null;
        System.out.println(arr.length);//运行后会报错——空指针异常
        //因为你不指向任何对象嘛，又怎么来谈长度呢
    }

    public static void hs1(int[]array1) {
        array1=new int[]{11,26,13,4,51,61};
    }

    public static void hs2(int[]array2) {
        array2[0]=211;
    }

    public static void main6(String[] args) {
        int []array={1,2,3,4,5,6};
        System.out.println(Arrays.toString(array));//[1, 2, 3, 4, 5, 6]
        hs1(array);
        System.out.println(Arrays.toString(array));//[1, 2, 3, 4, 5, 6]
        hs2(array);
        System.out.println(Arrays.toString(array));//[211, 2, 3, 4, 5, 6]
    }

    public static void main7(String[] args) {
        int []arr=new int[]{1,2,3,4,5};
        arr=new int[]{1};
        arr=new int[]{2};
        arr=new int[]{3};
        System.out.println(Arrays.toString(arr));//打印[3]
    }

    //数组作为方法的返回值
    //例：交换数组内两个数的值
    public static void swap(int[]array) {
        int tmp=array[0];
        array[0]=array[1];
        array[1]=tmp;
    }
    public static void main17(String[] args) {
        int []arr={1,2};
        System.out.println("交换前"+arr[0]+" "+arr[1]);
        swap(arr);
        System.out.println("交换后"+arr[0]+" "+arr[1]);
    }

    //写一个方法，将数组内元素*2
    public static void func(int[]arr) {
        for(int i=0;i<arr.length;i++)
        {
            arr[i]*=2;
        }
    }
    public static void main18(String[] args) {
        int []arr={1,2,3};
        func(arr);
        System.out.println(Arrays.toString(arr));
    }

    public static int[] func2(int []arr) {
       int []ret=new int[arr.length];
       for(int i=0;i<arr.length;i++)
       {
           ret[i]=2*arr[i];
       }
       return ret;
    }

    public static void main19(String[] args) {
        int []arr={1,2,3};
        int []brr=func2(arr);
        System.out.println(Arrays.toString(brr));
    }


    //写一个自己的ToString函数
    public static String myToString(int[] arr) {
        if(arr==null)
        {
            return null;//防止传过来的引用变量没有任何对象，那也就没有后续的.length说法了
        }
        String str="[";
        for(int i=0;i<arr.length;i++)
        {
            str=str+arr[i]+",";
        }
        str=str+"]";
        return str;
    }

    public static void main(String[] args) {
        int []arr={1,2,3};
        System.out.println(myToString(arr));
    }


    //例题：
    //键盘输入n个数,求这n个数的最大数和最小数并输出
    public static void main8(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("请问你要输入数字的个数？");
        int n=scanner.nextInt();
        int []arr=new int[n];
        int i=0;
        int j=0;
        System.out.println("请输入数字：");
        for(i=0;i<n;i++)
        {
            arr[i]=scanner.nextInt();
        }
        for (i = 0;i < n;i++)
        {
            for (j = 0;j < n - 1 - i;j++)//这里因为i是从0开始的，所以每轮排n-对应轮次即n-（i+1）
            {
                if (arr[j] < arr[j + 1])
                {
                    int tmp = arr[j+1];
                    arr[j + 1] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
        for(i=0;i<n;i++)
        {
            System.out.print(arr[i]+" ");
        }
    }


    //2.求一个3阶方阵的对角线上各元素之和。ps:三阶方阵3*3的矩阵
    public static void main9(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int[][]arr=new int[3][3];
        int i=0;
        int j=0;
        int sum=0;
        System.out.println("请输入3*3的方阵：");
        for(i=0;i<3;i++)//行
        {
            for(j=0;j<3;j++)
            {
                arr[i][j]=scanner.nextInt();
                if(i==j||i-j==2||j-i==2)//对角线下标00 11 22 02 20
                {
                    sum+=arr[i][j];
                }
            }
        }
        System.out.println(sum);
    }


    //3.找出4×5矩阵中值最小和最大元素，并分别输出其值及所在的行号和列号。
    public static void main10(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int[][]arr=new int[4][5];
        int i=0;
        int j=0;
        int sum=0;
        System.out.println("请输入4*5的方阵：");
        for(i=0;i<4;i++)//行
        {
            for(j=0;j<5;j++)
            {
                arr[i][j]=scanner.nextInt();
            }
        }
        int min=arr[0][0];
        int max=arr[3][4];
        int minRow=0,minColumn=0;//最小行与列
        int maxRow=0,maxColumn=0;//最大行与列
        for(i=0;i<4;i++)
        {
            for(j=0;j<5;j++)
            {
                if(arr[i][j]<=min) {
                    min=arr[i][j];
                    minRow=i;
                    minColumn=j;
                }
                if(arr[i][j]>=max) {
                    max=arr[i][j];
                    maxRow=i;
                    maxColumn=j;
                }
            }
        }
        maxRow++;
        maxColumn++;
        minRow++;
        minColumn++;//我们默认下标是0开始，对应实际要加1
        System.out.println("最大值为："+max+" 行号："+maxRow+" 列号："+maxColumn);
        System.out.println("最小值为："+min+" 行号："+minRow+" 列号："+minColumn);
    }
    //4.产生0~100的8个随机整数，并利用冒泡排序法将其升序排序后输出
    public static void main11(String[] args)
    {
        int[] num=new int[8];
        int i, j, temp;
        for(i=0; i<8; i++)
        {
            num[i]=(int)(Math.random()*100);
        }
        System.out.print("Random number: ");
        for(i=0; i<8; i++)
        {
            System.out.print(" "+num[i]+" ");
        }
        System.out.println();
        for(i=0; i<8; i++) {
            for (j = 0; j < 7 - i; j++) {
                if (num[j] > num[j + 1]) {
                    temp = num[j];
                    num[j] = num[j + 1];
                    num[j + 1] = temp;
                }
            }
        }
        System.out.print("Sort by: ");
                for(i=0; i<8; i++)
                    System.out.print(" "+num[i]+" ");
            }
         //5.编写Java应用程序，比较命令行中给出的两个字符串是否相等，并输出比较的结果。
         public static void main12(String[] args)
         {
             String str1, str2;
             Scanner reader=new Scanner(System.in);
             System.out.print("请输入第一个字符串: ");
             str1=reader.nextLine();
             System.out.print("请输入第二个字符串: ");
             str2=reader.nextLine();
             int cmp;
             cmp=str1.compareTo(str2);
             System.out.println("Result="+cmp);
         }
            //6.从键盘上输入一个字符串和子串开始的位置与长度，截取该字符串的子串并输出。
            public static void main13(String[]args) {
                String str;
                Scanner reader=new Scanner(System.in);
                System.out.print("请输入字符串: ");
                str=reader.nextLine();
                int sLocation, length;//sLocation表示子串开始位置，length表示长度
                System.out.print("请输入子串开始位置: ");
                sLocation=reader.nextInt();
                System.out.print("请输入子串长度: ");
                length=reader.nextInt();
                //写入划分长度 成为 length变量
                while((sLocation+length-1)>str.length())
                {
                    System.out.print("Error!Retry!\n");
                    System.out.print("Please enter length: ");
                    length=reader.nextInt();
                }
                String sub="";
                sub=str.substring(sLocation-1, sLocation+length-1);
                System.out.println("The new string: "+sub);
            }

            //7.从键盘上输入一个字符串和一个字符，从该字符串中删除给定的字符。
            public static void main14(String[] args) {
                Scanner reader=new Scanner(System.in);
                System.out.print("请输入字符串: ");
                String str=reader.nextLine();
                System.out.print("请输入你要删除的字符：");
                String c=reader.nextLine();
                str=str.replace(c, "");
                System.out.println("删除后字符串为: "+str);
            }
            //8.编程统计用户从键盘输入的字符串中所包含的字母、数字和其他字符的个数。
            public static void main15(String[] args)
            {
                Scanner reader=new Scanner(System.in);
                System.out.print("请输入字符串: ");
                String str=reader.nextLine();
                int i, n=str.length();
                int letterCount=0, numCount=0, otherCount=0;
                char ch;
                for(i=0; i<n; i++)
                {
                    ch=str.charAt(i);
                    if((ch>='a'&&ch<='z')||(ch>='A'&&ch<='Z'))
                        letterCount++;
                    if(ch>='0'&&ch<='9')
                        numCount++;
                    else
                        otherCount++;
                }
                System.out.println("字母个数="+letterCount);
                System.out.println("数字个数="+numCount );
                System.out.println("其他字符串个数="+(otherCount-letterCount));
            }
            //9.将用户从键盘输入的每行数据都显示输出，直到输入"exit"字符串，程序运行结束。
            public static void main16(String[] args)
            {
                Scanner reader=new Scanner(System.in);
                System.out.print("请输入字符串: ");
                String str=reader.nextLine();
                String s="exit";
                int cmp=str.compareTo(s);
                while(cmp!=0)
                {
                    System.out.println("现字符串: "+str);
                    System.out.print("请输入新字符串: ");
                    str=reader.nextLine();
                    cmp=str.compareTo(s);
                }
                System.out.println("程序已退出");
            }
            //10.定义一个载具类（MyViechle）,该类应包含载具类型、载具名称、燃料类型、最大速度、目前速度等成员变量以及加油、检测当前速度等操作。定义一个测试类，在测试类中创建载具类变量并初始化，测试调用各个成员变量以及成员方法。

}
