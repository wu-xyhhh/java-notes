public class TestDemo {
    /*public static void main(String[] args) {
        System.out.println("hello");
        boolean flg1=true;//非0为真，0为假
        System.out.println(flg1);//java中布尔类型只有两种情况：true或false
        boolean flg2=false;
        System.out.println(flg2);
        //JVM没有明确规范布尔类型的大小。有些书说1bit有些书说1字节
        //布尔类型不能进行数值运算，比如flag+1这种都是不行的
    }*/


    /*public static void main(String[] args) {
        String str="hello";
        System.out.println(str);
        String str2="world";
        System.out.println(str+str2);//可以把两个字符串拼接起来
        System.out.println(str+10+20);//其他数据类型与字符串类型使用+拼接，结构是一个字符串
        //打印hello1020，10跟着hello已经变成字符串了，所以不会与20相加
        System.out.println(10+30+str);
        //打印40hello，解释：因为计算机读这段代码是从左到右，两int型还没变成字符串算起来是40
        System.out.println(str+(10+20));//如果要打印hello30，加个括号即可
        System.out.println("a="+10+",b="+20);//a=10,b=20
        String s1="\"bit\"";//现在要打印”bit“，只要对两边的双引号转义即可
        System.out.println(s1);
    }*/

    /*public static void main(String[] args) {
        int a=10;
        {
            a=99;
        }
        System.out.println(a);//打印99
        {
            int b;
        }
        //System.out.println(b);//无法打印b，已经出了b的作用域
        final int c=10;//final修改的变量就不能修改了，类似c语言中的const
        //c=2;//c不可修改
        System.out.println(c);
    }*/

    /*public static void main(String[] args) {
        int a=10;//int是4字节
        long b=a;//long是8字节
        System.out.println(b);//4字节赋给8字节没有问题
        long c=12;
        //int d=c;//8字节赋给4字节会报错
        int d=(int)c;//强制类型转换后才能赋值
        boolean e=true;
        //int f=(int)e;//两个毫不相关的类型就算是强转也不能赋值

    }*/

    /*public static void main(String[] args) {
        byte a=1;
        byte b=2;
        //byte c=a+b;这里会报错：小于4字节运算时会把变量自动提升到整形，整形后赋给byte是不可以的
        byte c=(byte)(a+b);//要赋值a+b要对他们的结果强制类型转换
        byte d=1+2;//这里为什么不报错呢？1和2都是常量，程序编译的时候1+2就已经编译成3了
        //也就是说上一行代码==byte d=3;
        int a1=10;
        long a2=20;
        //int a3=a1+a2;这里运算时，小类型会提升为大类型，这里a1会提升到long类型，直接写会报错
        int a3=(int)(a1+a2);//必须对结果进行强转
        String e=String.valueOf(a1);//把int型转换为String类型
        System.out.println(e);
        //整数变字符串法二
        String f=a1+"";//整数加字符串就变字符串了
        System.out.println(f);
    }*/

    /*public static void main(String[] args) {
        int a=1;
        int b=2;
        int c=0;
        System.out.println(a/b);//打印0，如果需要小数，要有double
        System.out.println(a/c);//0是不能作为除数的，这里会异常
        System.out.println("hhh");//由于上一行代码异常了，所以从上面一段开始往后都不执行了
    }*/


    /*public static void main(String[] args) {
        System.out.println(10%3);//打印1
        System.out.println(-10%3);//-1
        System.out.println(10%-3);//1
        System.out.println(-10%-3);//-1
        System.out.println(11.5%3);//2.5
    }*/

    /*public static void main(String[] args) {
        int a=10;
        a+=1;//a=a+1
        System.out.println(a);//打印11
        short b=1;
        //short c=b+1;//直接这样写会报错，因为单独一个1默认是整形，整形与short型的b相加会提升到整形，直接赋值会出错
        short c=(short)(b+1);
        short s=10;
        s+=8;//会自动强制类型转换
        System.out.println(s);
    }*/

    /*public static void main(String[] args) {
        int a=10;
        int a2=a++;
        System.out.println(a);//打印11
        System.out.println(a2);//打印10
        int b=10;
        int b2=++b;
        System.out.println(b);//打印11
        System.out.println(b2);//打印11
        short x =128;
        byte y =(byte) x;
        System.out.println(x);
        System.out.println(y);
    }*/

    /*public static void main(String[] args) {
        int a=10;
        int b=20;
        System.out.println(a==b);//false
        System.out.println(a!=b);//true
        System.out.println(a<b);//true
        System.out.println(a>b);//false
        System.out.println(a<=b);//true
        System.out.println(a>=b);//false
    }*/


    /*public static void main(String[] args) {
        int a=10;
        int b=20;
        int c=30;
        System.out.println(a<b&&b<c);//a<b成立且b<c成立，所以式子整体成立，打印true
        System.out.println(a>b&&b<c);//a>b不成立，不管b<c是否成立，式子整体肯定是false
    }*/


    /*public static void main(String[] args) {
        int a=10;
        int b=20;
        int c=30;
        System.out.println(a<b||b>c);//a<b成立就不用再判断b>c是否成立了，式子整体肯定为true
    }*/

    /*public static void main(String[] args) {
        int a=10;
        int b=20;
        //System.out.println(!a<b);
        System.out.println(!(a<b));
    }*/

    /*public static void main(String[] args) {
        int a=10;//0000 1010
        int b=20;//0001 0100
        //按位与：   0000 0000 对应的数字为0
        System.out.println(a&b);//打印0
    }*/

    /*public static void main(String[] args) {
        int a=10;//0000 1010
        int b=20;//0001 0100
        //按位或：   0001 1110 对应的数字为30
        System.out.println(a|b);//打印30
    }*/

    /*public static void main(String[] args) {
        int a=10;//0000 1010
        int b=30;//0001 1110
        //按位异或   0001 0100 对应的数字为20
        System.out.println(a^b);//打印20
    }*/

    /*public static void main(String[] args) {
        int a=10;//0000 0000 0000 0000 0000 0000 00001010
        //按位取反  1111 1111  1111 1111  1111 1111  1111 0101
        //16进制打印 f    f     f    f      f    f    f    5
        //ps：4位2进制是16种情况，对应16进制的16个数
        System.out.printf("%x\n",~a);
    }*/

    /*public static void main(String[] args) {
        int a=11;
        System.out.println(a<<1);
        System.out.println(a<<2);
    }*/

    /*public static void main(String[] args) {
        int a=11;
        int b=-11;
        System.out.println(a>>1);//打印5
        System.out.println(b>>1);//打印-6
    }*/

   /* public static void main(String[] args) {
        int a=138;
        System.out.println(a>>>1);//打印69
    }*/

    public static void main(String[] args) {
        int a=10;
        int b=20;
        int max=a>b?a:b;//a>b不成立，执行b，也就说b赋值给max
        System.out.println(max);//打印20
    }
}
