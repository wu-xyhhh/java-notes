import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-01-11
 * Time: 19:56
 */
public class TestDemo {
    /*//字符与字符串
    public static void main(String[] args) {
        //1.public String(char value[])
        char[] val1={'a','b','c'};//字符数组里存放a，b，c三个字符
        String str1=new String(val1);//将字符数组中的所有内容变为字符串
        System.out.println(str1);//打印abc

        //2.public String(char value[],int offset,int count)
        //offset是偏移量的意思，count表示你要从偏移量为offset的位置取多少个
        char[] val2={'a','b','c','d','e'};
        //比如这里的a，就是数组偏移量为0的位置，b是数组偏移量为1的位置
        String str2=new String(val2,1,3);
        //表示从字符数组val2中偏移量为1的位置，取三个字符构成一个字符串str2
        //对于偏移量，你理解为数组下标也可以
        System.out.println(str2);//打印bcd

        //3.public char charAt(int index)
        //取得指定索引位置的字符，索引从0开始
        String str3="hello!";
        char ch1=str3.charAt(1);//获取1下标的字符
        char ch2=str3.charAt(2);//获取2下标的字符
        System.out.println(ch1);//打印e
        System.out.println(ch2);//打印l

        //ps:关于2和3，如果下标越界会报警告

        //4.public char[] toCharArray()
        //将字符串变为字符数组返回
        char[] chars=str3.toCharArray();
        //把str3指向的字符串对象变为字符数组,我们用chars数组来接收
        System.out.println(Arrays.toString(chars));//打印[h, e, l, l, o, !]
    }*/

    //给定一个字符串，判断其是否全部由数字组成
    //思路：将字符串变为字符数组而后判断每一位字符是否是“0”~“9”之间的内容，如果是则为数字
    /*public static boolean isNumberChar(String s) {
        for(int i=0;i<s.length();i++){
            char c=s.charAt(i);
            if(c<'1'||c>'9'){
                return false;
            }
        }
        return true;
    }
    public static void main(String[] args) {
        String str1="1234567";
        System.out.println(isNumberChar(str1));//打印true
        String str2="123abc456";
        System.out.println(isNumberChar(str2));//打印false
    }*/

    //字节与字符串
    /*public static void main(String[] args) throws UnsupportedEncodingException {
        //1.public String(byte bytes[])
        //将字节数组变为字符串
        byte[]bytes={97,98,99,100};
        String str1=new String(bytes);
        System.out.println(str1);//打印abcd——4个数字对应Ascii码为abcd

        //2.public String(byte bytes[],int offset,int length)
        //将部分字节数组中的内容变为字符串
        String str2=new String(bytes,1,3);//相当于从下标1开始取，取3个
        System.out.println(str2);//打印bcd

        //3.public byte[] getBytes()
        //将字符串以字节数组的形式返回
        byte []bytes1=str1.getBytes();
        System.out.println(Arrays.toString(bytes1));//会将str1中的abcd转换成对应的Ascii码
        //打印[97, 98, 99, 100]


        //4.public byte[] getBytes(String charsetNmae)throw UnsupportedEncodingException
        //编码转换处理
        byte []bytes2=str1.getBytes("utf-8");//ps：这里要抛一个异常
        //utf-8表示根据utf-8进行编码，你也可以改成GBK等其他的，
        // 需要注意的是，如果非英文的其他字符，编码情况不一样，打印的结果也是不一样的
        System.out.println(Arrays.toString(bytes2));//打印[97, 98, 99, 100]
        String str3="你好,哈哈哈";
        bytes2=str3.getBytes("utf-8");
        System.out.println(Arrays.toString(bytes2));
        //打印[-28, -67, -96, -27, -91, -67, 44, -27, -109, -120, -27, -109, -120, -27, -109, -120]
    }*/


    //字符串比较
   /* public static void main(String[] args) {
        //1.public boolean equals(Object anObject)
        //区分大小写的比较
        String str1="hello";
        String str2="Hello";
        System.out.println(str1.equals(str2));//打印false

        //2.public boolean equalsIgnoreCase(String anotherString)
        //不区分大小写的比较
        System.out.println(str1.equalsIgnoreCase(str2));//打印true

        //3.public int compareTo(String anotherString)
        //比较两个字符串大小关系,返回差值
        System.out.println(str1.compareTo(str2));//32
        //ASCII码：小写h 104，大写H 72
        //返回104-72，也就是32
        //ps:结果大于0，说明str1比str2大; 小于0，说明str1比str2小; 等于0，两者相同;
    }*/

    //字符串查找
    /*public static void main(String[] args) {
        //1.public boolean contains(CharSequence s)
        //判断一个子字符串是否存在
        String str="ababcab";
        String tmp="abc";
        boolean flg=str.contains(tmp);//如果str中包含tmp，则会返回第一个tmp出现的位置
        System.out.println(flg);//返回true


        //2.public int indexOf(String str)
        //从头开始查找指定字符串的位置，查到了返回位置的开始索引，如果查不到返回-1
        int index=str.indexOf(tmp);
        System.out.println(index);//打印2，尽管str开头两个是ab，但我们要的是整个abc，所以是下标2的位置
        //indexOf类似于C的strstr

        //3.public int indexOf(String str,int fromIndex)
        //从指定位置开始查找子字符串位置
        int index1=str.indexOf(tmp,2);//从str下标2的位置开始找tmp
        int index2=str.indexOf(tmp,3);//从str下标3的位置开始找tmp
        System.out.println(index1);//打印2，从下标2的位置开始找，可以找到
        System.out.println(index2);//打印-1，从下标3的位置开始找，找不到


        //4.public int lastIndexOf(String str)
        //从后向前查找子字符串位置
        int index3=str.lastIndexOf(tmp);
        System.out.println(index3);//打印2

        //5.public int lastIndexOf(String str,int fromIndex)
        //从指定位置由后向前查找
        int index4=str.lastIndexOf(tmp,5);//从下标5从后往前找
        int index5=str.lastIndexOf(tmp,1);//从下标1从后往前找
        System.out.println(index4);//打印2
        System.out.println(index5);//打印-1

        //6.public boolean startsWith(String prefix)
        //判断是否以指定字符串开头
        System.out.println(str.startsWith("a"));//判断str是否以“a”开头，打印true
        System.out.println(str.startsWith("b"));//判断str是否以“b”开头，打印false

        //7.public boolean startsWith(String prefix,int toffset)
        //从指定位置开始判断是否以指定字符串开头
        System.out.println(str.startsWith("c", 2));//打印false
        //判断str从下标2开始，是否以c开头——也就是判断下标2是否是“c”
        System.out.println(str.startsWith("a", 2));//打印true


        //8.public boolean endsWith(String suffix)
        //判断是否以指定字符串结尾
        System.out.println(str.endsWith("ac"));//str是否以字符串ac结尾,打印false
        System.out.println(str.endsWith("ab"));//str是否以字符串ab结尾，打印true
    }*/


    //字符串替换
    /*public static void main(String[] args) {
        //1.public String replaceAll(String regex,String replacement)
        //替换所有指定内容
        String str="ababcabcdc";
        String ret=str.replace('a','x');
        System.out.println(str);//打印ababcabcdc
        //原先的str是不变的——如果替换的字符不同，返回的是新的对象
        //比如str.replace('a','x');这里替换的‘a’和’x‘不同，所以会返回新的对象
        //比如str.replace('a','a');这里替换的‘a’和’a‘相同，所以会返回str本身
        System.out.println(ret);//打印xbxbcxbcdc

        String ret1=str.replace("ab","hh");
        System.out.println(ret1);//打印hhhhchhcdc

        String ret2=str.replaceAll("ab", "hh");
        System.out.println(ret2);//打印hhhhchhcdc
        //综上：replace可以替换字符或字符串，replaceAll只能替换字符串，其效果与replace一样


        //2.public String replaceFirst(String regex,String replacement)
        //替换首个内容
        String ret3=str.replaceFirst("ab","hh");
        System.out.println(ret3);//打印hhabcabcdc

    }*/

    //字符串拆分
    /*public static void main(String[] args) {
        //1.public String[]split(String regex)
        //将字符串全部拆分
        String str="my name is zhangSan&age 19";
        String[] strings=str.split("&");//分割线为&
        for(String s:strings){
            System.out.println(s);
        }//打印my name is zhangSan
        // age 19

        //拆分的特殊情况
        String str1="192.168.1.1";
        strings=str1.split(".");
        for(String s1:strings){
            System.out.println(s1);
        }//你会发现这里什么也没有打印
        //点号“.”比较特殊，如果要识别"."你在点号前要转义加\,
        // 但是\.又是另一层意思，我们要再加一个\
        //也就是\\.
        strings=str1.split("\\.");
        for(String s2:strings){
            System.out.println(s2);
        }//成功打印192 168 1 1
        //类似“.”这种还有“+” “*” “|” 以它们为分割线的时候，前面都要加\\

        //2.public String[]split(String regex,int limit)
        //将字符串部分拆分，该数组长度就是limit极限
        strings=str1.split("\\.",2);//以“.”为分割线，分割2组
        for(String s3:strings){
            System.out.println(s3);
        }
        //打印192
        //168.1.1

        //ps:这里的limit是最多分几组，而不是一定分这么多组
        strings=str1.split("\\.",5);//以“.”为分割线，分割2组
        for(String s4:strings){
            System.out.println(s4);
        }
        //打印192
        //168
        //1
        //1
        //一共以“.”分割，最多只能分4组，你传大于4的数据进去也只能分4组

        //ps:当一个字符串中不需要的符号过多怎办？
        //eg:
        String str2="ni hao@ hhh& ooo x";
        strings=str2.split(" |@|&");//如果要划分的分隔符比较多，中间用|隔开
        for(String s5:strings){
            System.out.println(s5);
        }
        //打印ni
        //hao
        //
        //hhh
        //
        //ooo
        //x
    }*/

    //字符串的截取
    /*public static void main(String[] args) {
        //1.public String substring(int beginIndex)
        //从指定位置索引截取到结尾
        String str="abcdefg";
        String sub=str.substring(2);//从下标2的位置开始，提取子串（一直到最后位置结束）
        System.out.println(sub);//打印cdefg

        //2.public String substring(int beginIndex,int endIndex)
        String sub1=str.substring(2,4);//从下标2开始提取子串，到下标4结束（不包含下标4)——左闭右开
        System.out.println(sub1);//打印cd
    }*/


    //其他操作方法
    /*public static void main(String[] args) {
        //1.public String trim()
        //去掉字符串中的左右空格，保留中间空格
        String str="   hhh   niHao   ";
        String ret=str.trim();
        System.out.println(ret);//打印hhh   niHao
        System.out.println(str);//str不变还是   hhh   niHao


        //2.public String toUpperCase()
        //字符串转大写
        String str1="AbcD";
        ret=str1.toUpperCase(Locale.ROOT);
        System.out.println(ret);//打印ABCD


        //3.public String toLowerCase()
        //字符串转小写
        ret=str1.toLowerCase(Locale.ROOT);
        System.out.println(ret);//打印abcd

        //4.public native String intern()
        //字符串入池操作
        //常量池有，就取常量池里的东西，没有就把这个对象放到常量池里


        //5.public String concat(String str)
        //字符串连接，等同于“+”,不入池
        //不入池是 拼接的结果不入池
        String str2="i love ";
        ret=str2.concat("china");
        System.out.println(ret);//打印i love china

        //6.public int length()
        //获取字符串长度
        String str3="we are family";
        System.out.println(str3.length());//打印13

        //7.public boolean isEmpty()
        //判断是否为空字符串，但不是null,而是长度为0
        System.out.println(str3.isEmpty());//false
        String str4="";
        System.out.println(str4.isEmpty());//true
    }*/

    //StringBuffer和StringBuilder
    /*public static void main(String[] args) {
        //1.append
        StringBuilder sb=new StringBuilder();
        sb.append("abcdefg");
        System.out.println(sb.toString());//打印abcdefg
        sb.append("123");
        System.out.println(sb.toString());//打印abcdefg123,自身会随之改变

        //ps:append()方法可以连用
        StringBuilder sb1=new StringBuilder();
        sb1.append("123").append("abc");
        System.out.println(sb1);//打印123abc

        //2.reverse
        //ps:StringBuild和StringBuffer中有reverse方法，String是没有的
        //用该方法可以非常轻松的实现字符串的逆置
        sb.reverse();//逆置
        System.out.println(sb);//打印321gfedcba
    }*/

    //StringBuffer或StringBuilder转换为String
    public static String func(){
        StringBuffer stringBuffer=new StringBuffer();
        return stringBuffer.toString();
    }


    //String转换为StringBuffer或StringBuilder
    public static StringBuffer func(String str){
        StringBuffer stringBuffer=new StringBuffer();
        stringBuffer.append(str);
        return stringBuffer;
    }

}
