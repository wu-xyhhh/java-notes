import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-02-19
 * Time: 19:33
 */
public class TestDemo {
    //Map和Set:可以在查找时，进行一些插入和删除操作（动态查找）

    //两种模型：
    //1.纯key模型(Set)
    //eg:查找某个名字是否在通讯录中

    //2.key-value模型(Map)
    //eg:及时雨-宋江（及时雨是key，value是宋江）


    //Map常用方法
    public static void main0(String[] args) {
        Map<String,Integer>map=new HashMap<>();
        map.put("abc",3);//put就是往Map里面放元素的方法
        map.put("bit",2);
        map.put("hello",4);
        System.out.println(map);//打印{abc=3, hello=4, bit=2}
        //存放元素顺序和打印顺序不一样
        //HashMap存储元素时是根据某个函数来决定的，该函数为哈希函数（后面会学习到）

        int ret=map.get("bit");//get方法可以通过key值获得value值
        System.out.println(ret);//打印2
        System.out.println(map.get("bit3"));//打印null，没有bit3

        System.out.println(map.getOrDefault("bit",100));//打印2
        System.out.println(map.getOrDefault("bit3",100));//打印100
        //getOrDefault方法就是，有就打印value，没有返回给定的值，比如上面的100

        Integer ret2=map.remove("bit");
        System.out.println(ret);//打印2
        System.out.println(map);//打印{abc=3, hello=4}

        //原先有{abc=3, hello=4}
        map.put("hello",2);//ps:put在存储元素时，key如果相同，value值会被新的覆盖
        System.out.println(map);//{abc=3, hello=2}

        Set<String> set=map.keySet();//keySet方法：返回所有key的不重复集合，返回值类型为Set
        //ps:Set里面存储的元素都是不重复的-Set是集合，集合内元素不可重复
        System.out.println(set);//打印[abc, hello]


        Set<Map.Entry<String,Integer>> entrySet=map.entrySet();
        //Set里面放的是Map.Entry<String,Integer>类型
        for(Map.Entry<String,Integer>entry:entrySet){
            System.out.println(entry.getKey()+"--"+entry.getValue());
        }//打印abc--3
        //  hello--2

        map.put(null,null);//这句代码不会报错，编译完成也没有问题
        //但是如果Map<String,Integer>map=new HashMap<>();
        Map<String,Integer>map2=new TreeMap<>();
        map2.put(null,null);//这里编译会报错
        //ps：放入TreeMap里的数据，一定是可以比较的

        //注意事项：
        //1. Map是一个接口，不能直接实例化对象，如果要实例化对象只能实例化其实现类TreeMap或者HashMap
        //2. Map中存放键值对的Key是唯一的，value是可以重复的
        //3. 在Map中插入键值对时，key不能为空，否则就会抛NullPointerException异常，但是value可以为空
        //4. Map中的Key可以全部分离出来，存储到Set中来进行访问(因为Key不能重复)。
        //5. Map中的value可以全部分离出来，存储在Collection的任何一个子集合中(value可能有重复)。
        //6. Map中键值对的Key不能直接修改，value可以修改，如果要修改key，只能先将该key删除掉，然后再来进行重新插入。
    }

    //Set常用方法
    /*public static void main(String[] args) {
        Set<Integer> set=new HashSet<>();
        //Set会自动对存入的数据进行去重处理
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(3);
        set.add(4);
        System.out.println(set);//打印[1, 2, 3, 4]

        Iterator<Integer>iterator=set.iterator();//返回类型为迭代器
        while(iterator.hasNext()){
            System.out.print(iterator.next()+" ");
        }//打印1 2 3 4



        //注意：
        *//*1. Set是继承自Collection的一个接口类
        2. Set中只存储了key，并且要求key一定要唯一
        3. Set的底层是使用Map来实现的，其使用key与Object的一个默认对象作为键值对插入到Map中的
        4. Set最大的功能就是对集合中的元素进行去重
        5. 实现Set接口的常用类有TreeSet和HashSet，还有一个LinkedHashSet，LinkedHashSet是在HashSet的基础
        上维护了一个双向链表来记录元素的插入次序。
        6. Set中的Key不能修改，如果要修改，先将原来的删除掉，然后再重新插入
        7. Set中不能插入null的key。*//*

    }*/


    //1.给定1w个数据，统计每个数据出现的次数
    //2.给定1w个数据，把重复的数据去除掉
    //3.给定1w个数据，找出第一个重复的数据
    public static Map<Integer,Integer> func1(int[] arr) {
        Map<Integer,Integer> map=new HashMap<>();
        //判断arr中的元素原先是否在map中，如果不在就是1（原先没有，现在有了），在就是在原来的基础上+1
        for(int i=0;i< arr.length;i++){
            if(map.containsKey(arr[i])){
                map.put(arr[i],map.get(arr[i])+1);
            }else{
                map.put(arr[i],1 );
            }
        }
        return map;
    }

    public static Set<Integer>func2(int[] array){
        HashSet<Integer>set=new HashSet<>();
        for(int x:array){
            set.add(x);
        }
        return set;
    }


    public static int func3(int[] arr) {
        HashSet<Integer> set=new HashSet<>();
        for(int x:arr){
            if(set.contains(x)){
                return x;
            }
            set.add(x);
        }
        return -1;
    }



    //3个函数测试
    public static void main(String[] args) {
        int[]arr=new int[10000];
        Random random=new Random();
        for(int i=0;i<arr.length;i++){
            arr[i]=random.nextInt(100);//数据范围0-100
        }
        Map<Integer,Integer> map=func1(arr);
        System.out.println(map);
    }





}
