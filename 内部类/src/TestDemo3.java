import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-02-23
 * Time: 19:32
 */
//4.匿名内部类

class Test{
    public void test(){
        System.out.println("匿名内部类方法test");
    }
}
public class TestDemo3 {
    public static void main(String[] args) {
        new Test(){

        }.test();//打印匿名内部类方法test

        new Test().test();//这种写法和上面效果一样

        new Test(){
            @Override
            public void test(){
                System.out.println("我是重写的test方法");
            }
        }.test();



    }
}
