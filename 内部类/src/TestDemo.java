/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-02-22
 * Time: 22:05
 */
//内部类：

    class OuterClass{//外部类
        public int data1=1;
        public int data2=2;
        public static int data3=3;

        public void func1(){
            System.out.println("OuterClass:func");
        }

        class InnerClass{//2.实例内部类——你可以把它当做外部类的实例成员变量
            //别的类里面的类
            public int data1=99;//和外部类成员变量同名
            public int data4=4;
            private int data5=5;
            //public static int data6=6;
            // 这里static会报错，静态是不属于对象的，静态是属于类的
            //而内部类是外部类的成员变量，是依赖对象的，两者冲突

            //因为内部类也是类，所以也可以写构造方法，和普通成员方法
            public InnerClass(){
                System.out.println("不带参数的内部类方法");
            }

            public void test(){
                System.out.println(data1);//data1和外部类同名，调用时用内部类的
                System.out.println(this.data1);//上一行代码的另一种写法
                System.out.println(OuterClass.this.data1);//这样写用外部类的同名变量

                System.out.println(data2);
                System.out.println(data3);
                System.out.println(data4);
                System.out.println(data5);
                System.out.println("InnerClass普通方法");
            }

            //ps:内部类里面还可以有内部类，原理同上
        }
}
//实例内部类，使用范例
class MyLinkedList{
        class Node{
            //...
        }
}//我们没学内部类前，链表和节点是两个类，其实可以放一起
//内部类经常使用情况就是:...由...组成






public class TestDemo {
    //1.本地内部类：定义在方法中的内部类
    public void func(){
        class test{
            public int a;
        }//缺点：只能在当前方法中使用——很少使用本地内部类，了解即可
    }


        //获取实例内部类对象
        public static void main(String[] args) {
            OuterClass outerClass=new OuterClass();
            outerClass.func1();

            //InnerClass是OuterClass的普通成员变量，它的调用需要外部类.
            //实例化 实例内部类对象
            OuterClass.InnerClass innerClass=outerClass.new InnerClass();
            //运行是先外部类的func，然后是内部类的方法
            innerClass.test();//打印InnerClass普通方法和 99 99 1 2 3 4 5
            //ps:内部类也是可以继承的，方法可以自行csdn
        }
}
