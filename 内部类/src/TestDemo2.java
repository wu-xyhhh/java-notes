/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-02-23
 * Time: 18:26
 */
//3.静态内部类

class OuterClass2{
    public int data1=1;
    private int data2=2;
    private static int data3=3;

    //静态内部类
    static class InnerClass{
        public int data4=4;
        private int data5=5;
        public static int data6=6;
        public OuterClass out=new OuterClass();
        public void test(){
            //System.out.println(data1);//data1和data2都会报错
            //System.out.println(data2);
            //data1和data2需要外部类对象使用
            //正确访问data1和data2方法：
            System.out.println(new OuterClass().data1);//法一
            System.out.println(out.data2);//法二
            System.out.println(data3);//data3-6都可以正常使用
            System.out.println(data4);
            System.out.println(data5);
            System.out.println(data6);
            System.out.println("Innerclass:test()");
        }
    }
}
public class TestDemo2 {
    public static void main(String[] args) {
        OuterClass2 out=new OuterClass2();
        //实例化 静态内部类对象
        OuterClass2.InnerClass innerClass=new OuterClass2.InnerClass();
        innerClass.test();//1 2 3 4 5 6
        //实例化静态内部类可以没有外部类对象
    }

}
