/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-11-11
 * Time: 9:24
 */


public class TestDemo {
    //合并两个有序链表
    //ps:合并后不可避免的会破坏原链表结构
    //比如链表一：12  23  34  45  56
    //   链表二: 13  24  30
    //合并后链表：12  13  23  24  30  34  45  56
    public static  ListNode mergeTwoLists(ListNode head1,ListNode head2){
        ListNode newHead=new ListNode(-1);
        ListNode tmp=newHead;
        while(head1!=null&&head2!=null){
            if(head1.val< head2.val){
                tmp.next=head1;
                head1=head1.next;
                tmp=tmp.next;
            }
            else{
                tmp.next=head2;
                head2=head2.next;
                tmp=tmp.next;
            }
        }
        if(head1!=null){
            tmp.next=head1;
        }
        if(head2!=null){
            tmp.next=head2;
        }
        return newHead.next;//newHead是个傀偶节点，真正第一个节点是newHead.next
    }

    public static void main(String[] args) {
        /*myLinkedList myLinkedList=new myLinkedList();
        myLinkedList.createList();//我们这里以穷举法创建链表
        myLinkedList.display();//打印当前链表
        ListNode y=myLinkedList.FindKthToTail(myLinkedList.head, 3);
        System.out.println(y.val);
        ListNode x=myLinkedList.middleNode();//打印当前链表的中间元素，用x来接收
        System.out.println(x.val);
        System.out.println(myLinkedList.contain(12));//判断当前链表中是否存在12
        System.out.println(myLinkedList.contain(31));
        System.out.println(myLinkedList.size());//计算链表长度
        myLinkedList.addFirst(1);//头插1
        myLinkedList.display();//打印当前链表
        System.out.println(myLinkedList.size());//计算链表长度
        myLinkedList.addLast(9);//尾插9
        myLinkedList.display();//打印当前链表
        System.out.println(myLinkedList.size());//计算链表长度
        myLinkedList.addIndex(3,7);//任意位置插入节点，第一个数据节点下标为0
        myLinkedList.display();//打印当前链表
        myLinkedList.deleteIndex(23);//删除链表中第一次出现的值key
        myLinkedList.display();//打印当前链表
        myLinkedList.deleteIndex(1);//删除链表中第一次出现的值key
        myLinkedList.display();//打印当前链表
        myLinkedList.deleteIndex(9);//删除链表中第一次出现的值key
        myLinkedList.display();//打印当前链表
        myLinkedList.deleteIndex(2);//删除链表中第一次出现的值key
        myLinkedList.display();//打印当前链表
        myLinkedList.addFirst(1);//头插1
        myLinkedList.addFirst(1);//头插1
        myLinkedList.addFirst(1);//头插1
        myLinkedList.addFirst(1);//头插1
        myLinkedList.display();//打印当前链表
        myLinkedList.deleteAllIndex(1);
        myLinkedList.display();//打印当前链表
        myLinkedList.nz();
        myLinkedList.display();//逆置完后，原先首节点12变成了最后一个节点，如果还是按原先的打印方法只能打印12这一个数
        myLinkedList.clear();;
        myLinkedList.display();//打印当前链表*/


        /*myLinkedList m=new myLinkedList();
        m.createList();
        myLinkedList y=new myLinkedList();
        y.createList2();
        ListNode j= mergeTwoLists(m.head,y.head);
        while(j!=null){
            System.out.print(j.val+" ");
            j=j.next;
        }*/

        /*myLinkedList myLinkedList=new myLinkedList();
        myLinkedList.createList3();
        myLinkedList.display();
        ListNode z=myLinkedList.partition(21);
        while(z!=null){
            System.out.print(z.val+" ");
            z=z.next;
        }*/

        /*myLinkedList myLinkedList=new myLinkedList();
        myLinkedList.createList4();
        boolean is=myLinkedList.chkPalindrome(myLinkedList.head);
        System.out.println(is);
        myLinkedList m2=new myLinkedList();
        m2.createList5();
        boolean is2= m2.chkPalindrome(m2.head);
        System.out.println(is2);
        myLinkedList m3=new myLinkedList();
        m3.createList6();
        boolean is3=m3.chkPalindrome(m3.head);
        System.out.println(is3);*/


        /*ListNode listNode1=new ListNode(1);
        ListNode listNode2=new ListNode(2);
        ListNode listNode3=new ListNode(3);
        ListNode listNode4=new ListNode(4);
        ListNode listNode5=new ListNode(5);
        listNode1.next=listNode2;
        listNode2.next=listNode3;
        listNode3.next=listNode4;
        listNode4.next=listNode5;
        listNode5.next=null;//1-2-3-4-5

        ListNode listNode6=new ListNode(6);
        ListNode listNode7=new ListNode(7);
        ListNode listNode8=new ListNode(8);
        listNode6.next=listNode7;
        listNode7.next=listNode8;
        listNode8.next=listNode3;//6-7-8-3-4-5

        myLinkedList myLinkedList=new myLinkedList();
        ListNode L= myLinkedList.getIntersectionNode(listNode1,listNode6);
        while(L!=null){
            System.out.println(L.val);
            L=L.next;
        }*/

        myLinkedList myLinkedList=new myLinkedList();
        myLinkedList.addLast(1);
        myLinkedList.addLast(2);
        myLinkedList.addLast(3);
        myLinkedList.addLast(4);
        myLinkedList.addLast(5);
        myLinkedList.createLoop();
        System.out.println(myLinkedList.hasCycle());

    }
}
