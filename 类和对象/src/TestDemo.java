/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-11-06
 * Time: 18:43
 */
class Test{
    public int a;
    public static int count;
    public final int size=1;//被final修饰的叫常量，也属于对象，后续不可更改，类似C语言const
    public final static int c=99;//静态的常量,属于类本身，有且只有一份，被final修饰后续不可更改
    public String name;
}
class Name{
    private String name;
    //一旦属性或方法被private修饰，则该属性或方法就被封装起来了
    //封装的效果就是：被封装的属性或方法只能在当前的类中使用

    public String getName() {//获取Name的方式
        return name;
    }

    public void setName(String myName){
        name=myName;
    }
    public void setName2(String name){
        this.name=name;//形参赋给属性，方法二
    }
}

class people{
    public String name;
    public people(){
        System.out.println("people()不带参数的构造方法");
    }
    public people(String name){
        this.name=name;
        System.out.println("people(String)带String参数的构造方法");
    }
    {
        System.out.println("实例代码块");
    }
    static{
        System.out.println("静态代码块");
    }
}
class Test2 {
    public static void hello() {
        System.out.println("hello");
    }
}

class person{

}

public class TestDemo {
    public static void func1() {

    }

    public void func2() {

    }

    /*public static void main0(String[] args) {
        func1();//静态方法可以直接调用
        //func2();//普通方法直接调用会报错
        //普通方法的 正确调用方式
        TestDemo test = new TestDemo();
        test.func2();
    }*/

    public static void main1(String[] args) {
        Test t = new Test();
        t.name = "abc";//没有被private修饰可以用
        Name n = new Name();
        //n.name="abc";//被private修饰了，你再用就会报错
        n.setName("xiaoMing");//外部给name赋值
        String x = n.getName();//外部接收name
        System.out.println(x);
    }

    public static void main2(String[] args) {

        people p = new people("xiaoHei");
    }

    public static int singleNumber(int[] nums) {
        int num = 0;
        for (int i = 0; i < nums.length; i++) {
            num ^= nums[i];
        }
        return num;
    }


    public static void main(String[] args) {
        people p1=new people();
        System.out.println("===============");
        people p2=new people();
    }



}

