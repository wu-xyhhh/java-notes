import java.util.Arrays;

class Person{//一个类由属性和字段组成
    //属性（也叫字段、成员变量）
    public  String name;//属性是可以赋值的，但不建议
    public int age=18;//不赋值也可以
    public static int count;//静态成员变量（也是成员变量）
    //方法（行为）
    public int num;
    public void eat(){
        System.out.println(name+"正在吃饭");
    }
    public void sleep(){
        System.out.println(name+"正在睡觉");
    }
    public static void xiao() {
        System.out.println("haha");
    }

}





public class test {


    public static void main12(String[] args) {
        int      a     =1;
        Person xiaoHei=null;
        //和上面int a =1一样，Person是一种类型，xiaoMing是一个变量，然后我们可以给它赋初值
        //注意：由类定义的变量是一个引用变量，初值可以给null
        Person  xiaoMing=new Person();//由Person这个类实例化一个对象

        //成员变量age怎么访问？
        System.out.println(xiaoMing.age);
        //我们前面说过xiaoMing是一个引用变量，引用了Person这个对象
        //对象里包含了name、age等等成员变量
        //我们可以通过xiaoMing.age来访问age
        System.out.println(xiaoMing.name);//如果没有赋值直接打印是打印null
        System.out.println(xiaoMing.num);//如果没有赋值直接打印是打印0
        //解释：
        //整形：byte、short、int、long默认值0
        //浮点型：double、float默认值为0.0
        //字符型：char默认值"\u0000"
        //布尔型：boolean默认值false
        //引用数据类型：数组、类、接口默认值null

        //既然可以访问这些成员变量，我们就可以给它赋值
        xiaoMing.num=1000;
        System.out.println(xiaoMing.num);//打印1000
        xiaoHei=new Person();
        System.out.println(xiaoHei.num);
        //打印0,同一个类的创建的不同对象是不影响的，
        // 比如你给小明一个号码牌，不代表所有人类都有这样一个号码牌
        //其他人（对象）如果没有给num赋值，他们的num依旧默认是0

        //调用方法
        xiaoMing.eat();//引用变量名.方法() 即可


        //static关键字
        xiaoMing.count=1;
        System.out.println(xiaoMing.count);//打印1，好像和普通成员变量没有什么区别


    }

    public static void main13(String[] args) {
        Person p1=new Person();
        Person p2=new Person();
        p1.count++;
        p1.age++;
        System.out.println(p1.count);//打印1
        System.out.println(p1.age);//打印19——age原先赋值了18，++后变成了19

        p2.count++;
        p2.age++;
        System.out.println(p2.count);//打印2
        System.out.println(p2.age);//打印19
        Person.count++;
        System.out.println(Person.count);
        //打印3，count是类变量，你可以直接通过类名使用，
        // 不需要去new对象（节省创建对象的空间）
        Person.xiao();//打印haha
    }
























    //找出数组中最大元素
    public static int max(int[]array) {
        if(array==null||array.length==0)
        {
            return -1;
        }
        int max=array[0];
        for(int i=1;i< array.length;i++)
        {
            if(max<array[i])
            {
                max=array[i];
            }
        }
        return max;
    }
    public static void main0(String[] args) {
        int[]arr={12,9,13,1,0};
        int b=max(arr);
        System.out.println(b);
    }

    //数组中查找某个数，如果找到返回下标，否则返回-1
    public static int find(int[]array,int key) {//array是要查找的数组、key是要查的元素
        for(int i=0;i<array.length;i++)
        {
            if(array[i]==key)
            {
                return i;
            }
        }
        return -1;//没找到
    }
    public static void main1(String[] args) {
        int []arr={1,9,6,5,7};
        System.out.println(find(arr,6));
        System.out.println(find(arr,0));
    }


    //判断一个数组里元素是否有序（按升序标准）
    public static boolean isUp(int[]arr) {
        for(int i=0;i<arr.length-1;i++)
        {//这里i<arr.length-1是因为数组最后一个数下标是length-1，如果和arr.length比较是非法访问
            if(arr[i]>arr[i+1])
            {
                return false;
            }
        }
        return true;
    }
    public static void main2(String[] args) {
        int []arr={11,12,3,15,16};
        boolean f=isUp(arr);
        System.out.println(f);
        int []brr={1,2,3,4,5};
        boolean e=isUp(brr);
        System.out.println(e);
    }

    //数组（冒泡）排序
    public static void bubbleSort(int[]arr) {
        boolean f=false;
         for(int i=0;i<arr.length-1;i++)
         {
             for(int j=0;j<arr.length-1-i;j++)
             {
                 if(arr[j]>arr[j+1])
                 {
                     int tmp=arr[j];
                     arr[j]=arr[j+1];
                     arr[j+1]=tmp;
                     f=true;
                 }
             }
             if(f==false)
             {
                 return;//比如2134，走完一趟后变成1234，后续就不需要再继续判断了，可以节约很多运算时间
             }
         }
    }
    public static void main3(String[] args) {
        int []array={11,26,7,18,9,0};
        bubbleSort(array);
        System.out.println(Arrays.toString(array));
    }

    //数组元素的逆置比如1357->7531
    public static void nz(int[]arr) {
        int i=0;
        int j=arr.length-1;
        while(i<j)
        {
            int tmp=arr[i];
            arr[i]=arr[j];
            arr[j]=tmp;
            i++;
            j--;
        }
    }
    public static void main4(String[] args) {
        int []arr={1,3,5,7};
        nz(arr);
        System.out.println(Arrays.toString(arr));
    }


    //整形数组，偶数放在前半部分，奇数放在后半部分
    public static void change(int[]arr) {
        int i=0;
        int j= arr.length-1;
        while(i<j)
        {
            while(i<j&&arr[i]%2==0)
            {
                i++;
            }//走完while arr[i]为奇数
            while(i<j&&arr[j]%2!=0)
            {
                j--;
            }//走完while arr[j]为偶数
            int tmp=arr[i];
            arr[i]=arr[j];
            arr[j]=tmp;
        }
    }
    public static void main5(String[] args) {
        int[]arr={1,2,3,4,5,6};
        change(arr);
        System.out.println(Arrays.toString(arr));
    }

    public static int[] copy1(int[]arr) {
        int []copy=new int[arr.length];
        for(int i=0;i<arr.length;i++)
        {
            copy[i]=arr[i];
        }
        return copy;
    }
    public static void main6(String[] args) {
        int []arr={1,2,3,4,5};
        int []brr=copy1(arr);
        System.out.println(Arrays.toString(arr));
    }

    public static void main7(String[] args) {
        int []arr={0,9,1,4,5};
        int []brr=Arrays.copyOf(arr,arr.length);
        //第一个参数是被拷贝的数组，第二个参数是拷贝的长度，返回值类型int[]
        System.out.println(Arrays.toString(brr));//打印[0, 9, 1, 4, 5]

        int []crr=Arrays.copyOfRange(arr,1,3);//从arr下标1开始拷贝到下标3,左闭右开区间[1，3)
        System.out.println(Arrays.toString(crr));//打印[9,1]

        int []drr=new int[arr.length];
        System.arraycopy(arr,0,drr,0,arr.length);
        //第一个参数：原数组 第二个参数：从原数组下标哪里开始拷贝 第三个参数：从目标数组哪个位置拷贝进 第四个参数，拷贝长度
        System.out.println(Arrays.toString(drr));//打印[0, 9, 1, 4, 5]

        int []err=arr.clone();
        System.out.println(Arrays.toString(err));//打印[0, 9, 1, 4, 5]
    }


    //二维数组
    public static void main8(String[] args) {
        //法一
        int[][]arr={{1,2,3},{4,5,6}};
        //java中不允许int[][]这个[][]里加数字，这是一种类型
        //另外，你不能直接写{1,2,3,2,3}，必须你手动进行分行（加括号）

        //法二
        int[][]brr=new int[][]{{1,2,3},{4,5,6}};
        //这样的创建方式也可以，但和法一一样，new int[][]，[][]里不能加指定数字

        //法三
        int[][]crr=new int[2][3];
        //如果不进行初始化只是new一个对象可以new int[2][3]，
        // [][]里可以加数字（不然不知道你要开辟多大空间）
    }

    //打印二维数组，法一
    public static void main9(String[] args) {
        int[][]arr={{1,2,3},{4,5,6}};
        System.out.println(arr.length);//打印2，说明arr这个二维数组里存放2个引用变量，分别指向两个对象
        for(int i=0;i< arr.length;i++)//因为大多数情况int[][]是不告诉你有多少行多少列的，arr.length可以快速计算出行数
        {
            for(int j=0;j<arr[i].length;j++)
                //arr[i]进去arr里，也就是arr里的一个引用变量，
                // 然后引用变量.length计算出它指向对象的长度，也就是列数
            {
                System.out.print(arr[i][j]+" ");
            }
            System.out.println();
        }
    }

    //法二
    public static void main10(String[] args) {
        int[][]arr={{1,2,3},{4,5,6}};
        for(int[]ret:arr)//arr是一个一维数组，里存放2个引用变量，我们用一维数组ret来接受
        {
            for(int x:ret)//ret是一个一维数组，我们用x来接受数组里的数据
            {
                System.out.print(x+" ");
            }
            System.out.println();
        }
    }

    //法三
    public static void main11(String[] args) {
        int[][]arr={{1,2,3},{4,5,6}};
        System.out.println(Arrays.deepToString(arr));
    }

}


