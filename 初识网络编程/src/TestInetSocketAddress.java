import java.net.InetSocketAddress;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-12-27
 * Time: 9:20
 */
public class TestInetSocketAddress {
    public static void main(String[] args) {
        InetSocketAddress inetSocketAddress=new InetSocketAddress("127.0.0.1",8080);
        System.out.println(inetSocketAddress);
        System.out.println(inetSocketAddress.getAddress());//地址：/127.0.0.1
        System.out.println(inetSocketAddress.getHostName());//主机名：127.0.0.1
        System.out.println(inetSocketAddress.getPort());//端口：8080




    }
}
