import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-12-27
 * Time: 9:45
 */
//IP相当于某省、某市、某小区,Port端口相当于小区门牌号
//客户端
public class TcpClientDemo {
    public static void main(String[] args) throws IOException {
        Socket socket=null;
        OutputStream outputStream=null;
        try{
            //1.要知道服务器地址
            InetAddress serverIp=InetAddress.getByName("127.0.0.1");
            //2.知道端口号，这里自己定义的是9999
            int port=9999;
            //3.创建一个socket链接
             socket=new Socket(serverIp,port);
            //4.发送消息IO流
            outputStream=socket.getOutputStream();
            outputStream.write("你好吴星宇".getBytes());
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(outputStream!=null){
                try{
                    outputStream.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
            if(socket!=null){
                try{
                    socket.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }


    }
}
