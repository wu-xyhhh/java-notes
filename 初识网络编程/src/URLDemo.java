import java.lang.reflect.MalformedParameterizedTypeException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-12-27
 * Time: 10:45
 */
public class URLDemo {
    public static void main(String[] args) throws MalformedParameterizedTypeException, MalformedURLException {
        URL url=new URL("http://localhost:8080/helloworld/index.jsp?username=wxy&password=123");
        System.out.println(url.getProtocol());//协议
        System.out.println(url.getHost());//主机ip
        System.out.println(url.getPort());//端口
        System.out.println(url.getPath());//路径
        System.out.println(url.getFile());//文件
        System.out.println(url.getQuery());//参数

    }
}
