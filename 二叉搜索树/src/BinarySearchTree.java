/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-02-20
 * Time: 20:48
 */
class Node{
    public int val;
    public Node left;
    public Node right;
    public Node(int val){
        this.val=val;
    }
}
public class BinarySearchTree {
    public Node root=null;

    public Node search(int key){
        Node cur=root;
        while(cur!=null){
            if(cur.val<key){
                cur=cur.right;
            }else if(cur.val==key){
                return cur;
            }else{
                cur=cur.left;
            }
        }
        return null;//走到这里，说明上面while没找到，也就是没有这个数据
    }

    public boolean insert(int val){
        if(root==null){
            root=new Node(val);
            return true;
        }
        Node newNode=new Node(val);//要插入的节点
        Node cur=root;//用来遍历的节点
        Node parent=null;
        //这个节点是用来标记cur的位置的（方便知道最后插入节点parent左边还是右边）
        //你可以理解为，parent就是上一个cur位置
        while(cur!=null){
            if(cur.val< val){
                parent=cur;
                cur=cur.right;
            }else if(cur.val> val){
                parent=cur;
                cur=cur.left;
            }else{//两个数据相同（搜索二叉树中不存在相同数据的情况）
                return false;
            }
        }
        if(parent.val<val){
            parent.right=newNode;
        }else if(parent.val>val){
            parent.left=newNode;
        }
        return true;
    }


    public void remove(int key){
        Node cur=root;
        Node parent=null;
        while(cur!=null){
            if(cur.val==key){//找到要删除的数了
                removeNode(cur,parent);//进行删除操作
                break;
            }else if(cur.val<key){
                parent=cur;
                cur=cur.right;
            }else{
                parent=cur;
                cur=cur.left;
            }
        }
    }

    public void removeNode(Node cur,Node parent){
        if(cur.left==null){//cur没有左孩子节点
            if(cur==root){
                root=cur.right;
            }else if(cur==parent.left){
                parent.left=cur.right;
            }else if(cur==parent.right){
                parent.right = cur.right;
            }
        }else if(cur.right==null){//cur没有右孩子节点
            if(cur==root){
                root=cur.left;
            }else if(cur==parent.left){
                parent.left=cur.left;
            }else if(cur==parent.right){
                parent.right=cur.left;
            }
        }else if(cur.left!=null&&cur.right!=null){
            //在要删除节点的右子树找最小值
            //我们这里创建变量，target表示要找的最小值节点
            Node targetParent=cur;//记录target的上一个位置
            Node target=cur.right;
            while(target.left!=null){//只往左边找（二叉搜索树左值<根<右值）
                targetParent=target;
                target=target.left;
            }
            cur.val=target.val;//target值放到要删除节点去
            if(target==targetParent.left){//target节点在它双亲节点左边，比如文章图中75在90左边
                targetParent.left=target.right;
            }else if(target==targetParent.right){
                targetParent.right = target.right;
            }
        }
    }

    //测试

    //用中序遍历（左根右），如果是二叉搜索树，则中序遍历一定是有序的
    public void inOrder(Node root){
        if(root==null){
            return;
        }
        inOrder(root.left);
        System.out.print(root.val+" ");
        inOrder(root.right);
    }
    public static void main(String[] args) {
        int []arr={10,8,19,3,9,4,7};
        BinarySearchTree binarySearchTree=new BinarySearchTree();

        //测试插入数据
        for(int i=0;i<arr.length;i++){
            binarySearchTree.insert(arr[i]);
        }
        binarySearchTree.inOrder(binarySearchTree.root);
        //打印3 4 7 8 9 10 19
        System.out.println();

        //测试插入重复数据
        boolean b=binarySearchTree.insert(7);
        System.out.println(b);//打印false

        //测试删除数据
        binarySearchTree.remove(8);
        binarySearchTree.inOrder(binarySearchTree.root);
        //打印3 4 7 9 10 19 （仍然有序，且没有8这个值）


    }

}
