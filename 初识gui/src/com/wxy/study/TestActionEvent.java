package com.wxy.study;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-12-16
 * Time: 19:48
 */
class MyActionListen implements ActionListener{
//事件监听
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("测试");
    }
}
class MyMonitor implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("该按钮:"+e.getActionCommand()+"被点击");
        //getActionCommand()获取按钮信息
    }
}
public class TestActionEvent {
    //事件监听
    //按一个按钮，触发一个事件
/*    public static void main(String[] args) {
        Frame frame =new Frame();
        Button button=new Button();
        //addActionListener()方法需要一个ActionListener，我们这里构造一个
        MyActionListen myActionListen=new MyActionListen();
        button.addActionListener(myActionListen);

        frame.add(button,BorderLayout.CENTER);
        frame.setSize(300,300);
        frame.setVisible(true);
        windowClose(frame);

    }*/

    public static void main(String[] args) {
        //两个按钮实现同一个监听
        //以下面的开始和退出按钮举例

        Frame frame=new Frame("双按钮测试");
        Button button1=new Button("开始");
        Button button2=new Button("退出");
        MyMonitor myMonitor=new MyMonitor();
        button1.addActionListener(myMonitor);
        button2.addActionListener(myMonitor);
        frame.setSize(300,300);
        frame.setVisible(true);
        frame.add(button1,BorderLayout.NORTH);
        frame.add(button2,BorderLayout.SOUTH);
        frame.pack();
        windowClose(frame);
    }

    //关闭窗体的事件
    private static void windowClose(Frame frame){
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                System.exit(0);
            }
        });
    }
}
