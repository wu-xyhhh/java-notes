package com.wxy.study;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-12-16
 * Time: 19:11
 */
public class TestFlowLayout {
    //流式布局
    public static void main(String[] args) {
        //创建一个窗口
        Frame frame=new Frame();

        //添加组件——按钮
        Button button1=new Button("b1");
        Button button2=new Button("b2");
        Button button3=new Button("b3");

        //设置为流式布局

        frame.setLayout(new FlowLayout());//默认是居中（从上方开始）
        frame.setLayout(new FlowLayout(FlowLayout.LEFT));//这样就可以默认按钮从左到右

        //设置大小
        frame.setSize(200,200);

        //设置可视化
        frame.setVisible(true);

        //把按钮添加上去
        frame.add(button1);
        frame.add(button2);
        frame.add(button3);


    }
}
