package com.wxy.study;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-12-16
 * Time: 18:33
 */
class MyFrame extends Frame{
    static int id=0;//窗口id，ps：可能有多个窗口，需要一个构造器
    public MyFrame(int x,int y,int width,int height ,Color color){//xy表示初始位置，width和height表示窗口大小
        super("MyFrame+"+(++id));//调用父类方法
        setBounds(x, y, width, height);
        setBackground(color);
        setVisible(true);
    }

}
public class TestFrame2 {
    public static void main(String[] args) {
        //展示多个窗口
        //ctrl+d
        //复制当前行代码
        MyFrame myFrame1=new MyFrame(600,100,300,300,Color.blue);
        MyFrame myFrame2=new MyFrame(900,100,300,300,Color.green);
        MyFrame myFrame3=new MyFrame(1200,100,300,300,Color.red);
        MyFrame myFrame4=new MyFrame(1500,100,300,300,Color.black);





    }
}
