package com.wxy.study;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-12-17
 * Time: 13:40
 */
public class MyCalculator {
    public static void main(String[] args) {
        //wxy简易可视化计算器

        //总窗口
        Frame frame=new Frame("wxy计算器");//创建窗口
        frame.setLayout(new GridLayout(3,1));//窗口为3行1列式
        frame.setVisible(true);//可视化
        frame.setSize(300,400);//大小
        frame.setLocation(600,600);//位置
        frame.setBackground(Color.white);//背景颜色
        frame.addWindowListener(new WindowAdapter() {
            //重写点击窗口关闭时要做的事情（原方法里是空的，你需要自己重写）
            @Override
            public void windowClosing(WindowEvent e) {//可关闭
                super.windowClosing(e);
                System.exit(0);
            }
        });


        //按钮
        //数字按钮
        Button b1=new Button("1");
        Button b2=new Button("2");
        Button b3=new Button("3");
        Button b4=new Button("4");
        Button b5=new Button("5");
        Button b6=new Button("6");
        Button b7=new Button("7");
        Button b8=new Button("8");
        Button b9=new Button("9");
        Button b0=new Button("0");
        Button d=new Button(".");//小数点

        //操作按钮
        Button cheng=new Button("*");
        Button chu=new Button("/");
        Button jia=new Button("+");
        Button jiaN=new Button("-");
        Button dy=new Button("=");
        Button qiuYu=new Button("%");
        Button C=new Button("C");//归零
        Button zkh=new Button("(");//左括号
        Button ykh=new Button(")");//右括号




        //面板
        Panel p1=new Panel(new BorderLayout());//放结果数据
        p1.setBounds(0,0,300,100);
        Panel p2=new Panel(new GridLayout(5,4));//操作数字与操作符
        p2.setBounds(0,100,300,300);
        p1.setBackground(Color.CYAN);
        p2.setBackground(Color.WHITE);
        p2.add(zkh);
        p2.add(ykh);
        p2.add(C);
        p2.add(jia);
        p2.add(b1);
        p2.add(b2);
        p2.add(b3);
        p2.add(jiaN);
        p2.add(b4);
        p2.add(b5);
        p2.add(b6);
        p2.add(cheng);
        p2.add(b7);
        p2.add(b8);
        p2.add(b9);
        p2.add(chu);
        p2.add(b0);
        p2.add(d);
        p2.add(qiuYu);
        p2.add(dy);

        frame.add(p1,BorderLayout.NORTH);
        frame.add(p2,BorderLayout.SOUTH);


    }

}
