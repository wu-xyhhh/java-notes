package com.wxy.study;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-12-16
 * Time: 18:54
 */
//panel可以看成一个空间，但它必须依存frame猜可以存在
public class TestPanel {
    public static void main(String[] args) {
        Frame frame=new Frame();

        //布局的概念
        Panel panel=new Panel();

        //设置布局
        frame.setLayout(null);

        //设置坐标，大小
        frame.setBounds(600,600,300,300);
        frame.setBackground(Color.black);

        //Panel相对于frame设置坐标
        panel.setBounds(50,50,100,100);
        panel.setBackground(Color.CYAN);
        //和frame几乎一样，只不过panel是内嵌到frame里的

        frame.add(panel);

        frame.setVisible(true);//设置窗口可视化

        //以上代码显示出的窗口点❌都是无法关闭窗口的（除非你中止程序）
        //解决方法：添加监听事件
        //监听实践
        frame.addWindowListener(new WindowAdapter() {
            //重写点击窗口关闭时要做的事情（原方法里是空的，你需要自己重写）
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                System.exit(0);
            }
        });
    }
}
