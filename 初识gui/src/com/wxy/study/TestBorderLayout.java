package com.wxy.study;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-12-16
 * Time: 19:23
 */
public class TestBorderLayout {
    //东西南北中布局
    public static void main(String[] args) {
        Frame frame=new Frame("TestBorderLayout");

        Button east=new Button("East");
        Button west=new Button("West");
        Button north=new Button("North");
        Button south=new Button("South");
        Button centre=new Button("Center");

        frame.add(east,BorderLayout.EAST);
        frame.add(west,BorderLayout.WEST);
        frame.add(south,BorderLayout.SOUTH);
        frame.add(north,BorderLayout.NORTH);
        frame.add(centre,BorderLayout.CENTER);

        frame.setSize(300,300);
        frame.setVisible(true);
    }
}
