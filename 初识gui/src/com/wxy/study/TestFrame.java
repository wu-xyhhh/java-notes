package com.wxy.study;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-12-16
 * Time: 18:20
 */
public class TestFrame {
    public static void main(String[] args) {
        //创建一个窗口对象
        Frame frame=new Frame("wxy第一个java可视化窗口");

        //需要设置可见性
        frame.setVisible(true);

        //设置窗口大小
        frame.setSize(400,400);

        //设置背景颜色
        frame.setBackground(new Color(1,2,3));//3原色red、green、blue，可以自己调

        //弹出的初始位置
        frame.setLocation(300,300);

        //设置大小固定
        //frame.setResizable(false);//默认是true的，你填了false就无法改变了



    }
}
