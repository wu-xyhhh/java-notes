package com.wxy.study;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-12-16
 * Time: 19:34
 */
public class TestGridLayout {
    public static void main(String[] args) {
        Frame frame=new Frame();
        Button b1=new Button("b1");
        Button b2=new Button("b2");
        Button b3=new Button("b3");
        Button b4=new Button("b4");
        Button b5=new Button("b5");
        Button b6=new Button("b6");

        frame.setLayout(new GridLayout(3,2));//n行m列式布局，这里是3行2列
        frame.setVisible(true);
        frame.setSize(300,300);

        //ps：关于java自带的函数
        //frame.pack();//自动布局

        frame.add(b1);
        frame.add(b2);
        frame.add(b3);
        frame.add(b4);
        frame.add(b5);
        frame.add(b6);
    }
}
