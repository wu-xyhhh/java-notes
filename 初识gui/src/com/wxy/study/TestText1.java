package com.wxy.study;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-12-16
 * Time: 21:10
 */
class MyActionListener2 implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {

        TextField textField=(TextField)e.getSource();//获取一些资源
        System.out.println(textField.getText());//输出输入框的文本
    }
}
class MyFrame2 extends Frame{//前面其他文件写过MyFrame了，这里换个名字
    public MyFrame2(){
        TextField textField=new TextField();
        add(textField);

        //监听这个文本框输入的文字
        MyActionListener2 myActionListener2=new MyActionListener2();
        textField.addActionListener(myActionListener2);

        setVisible(true);
        pack();
    }
}

public class TestText1 {
    //输入框事件监听
    public static void main(String[] args) {
      MyFrame2 myFrame2=new MyFrame2();
    }
}
