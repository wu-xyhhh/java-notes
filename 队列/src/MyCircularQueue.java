/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-01-16
 * Time: 16:04
 */
//数组实现循环队列——看成一个环形
public class MyCircularQueue {
    public int[] elem;
    public int front;//队头下标
    public int rear;//队尾下标
    public MyCircularQueue(int k){//初始化——这种初始化是不用usedSize的方法，你如果用usedSize，下面还是new int[K]
        this.elem=new int[k+1];
        //这里为什么用K+1，因为我们rear是队尾元素的后一个下标，最后如果满了，
        //比如我们要放8个元素的队列，我们要new长度为9的队列，有一个长度必须空出来判断队列是空还是满的（你不用usedSize，就没法直接标记队列是空是满）
        //空的时候，比如队尾在下标0，队头也在下标0
        //满的时候，队尾在下标8，队头在下标0
    }

    public boolean enQueue(int val){//入队
        if(isFull()){//满了就入不了了
            return false;
        }
        this.elem[rear]=val;
        rear=(rear+1)%elem.length;//循环队列向后移动下标公式：（当前下标+偏移量）%数组长度
        //这里为什么不是rear++，比如这里数组长度为8，下标为0-7，你rear到7再入元素，rear应该到0了
        //但是如果rear++则rear是8，（rear+1）%8则是0
        return true;
    }

    public boolean deQueue(){//出队
        if (isEmpty()){//空了就出不了了
            return false;
        }
        front=(front+1)%elem.length;
        //这里也不能直接front++，比如这里数组长度为8，下标为0-7，到下标7的时候，你再出队，front应该变成0
        //这里也不用担心原先的元素还在里面，当rear走到之前front位置的时候，就会把之前元素覆盖掉了
        return true;
    }

    public int Front(){//得到队头元素
        if(isEmpty()){
            return -1;
        }
        return elem[front];
    }

    public  int Rear(){//得到队尾元素
        if (isEmpty()){
            return -1;
        }

        int index=0;
        if(rear==0){
            index=elem.length-1;
        }else {
            index=rear-1;
        }
        return elem[index];//这里不能直接return elem[rear-1],
        // 比如长度8的数组，下标为0-7，你队尾如果是0下标（队尾元素下标是7），你0-1得到的是-1，数组下标越界了
    }

    public boolean isEmpty(){
        return front==rear;
    }

    public boolean isFull(){
        //rear的下一个就是front
        if((this.rear+1)% elem.length==front){
            return true;
        }
        return false;
    }


    public static void main(String[] args) {
        MyCircularQueue myCircularQueue=new MyCircularQueue(8);//要一个长度为8的循环队列
        myCircularQueue.enQueue(1);
        myCircularQueue.enQueue(2);
        myCircularQueue.enQueue(3);
        myCircularQueue.enQueue(4);
        myCircularQueue.enQueue(5);
        myCircularQueue.enQueue(6);
        myCircularQueue.enQueue(7);
        myCircularQueue.enQueue(8);
        int rear=myCircularQueue.Rear();//得到队尾元素
        int front= myCircularQueue.Front();//得到队头元素
        System.out.println(rear);//打印8
        System.out.println(front);//打印1

        System.out.println("=====分割线=====");

        myCircularQueue.deQueue();//出队
        System.out.println(myCircularQueue.front);//原先front在下标0处，你出了一个元素1，front变为1，队头元素变为2
        System.out.println(myCircularQueue.Front());//打印队头元素2

        System.out.println("=====分割线=====");

        myCircularQueue.deQueue();//出队
        System.out.println(myCircularQueue.front);//front在下标1处，再出一个元素，front变为2，队头元素变为3
        System.out.println(myCircularQueue.Front());//打印队头元素3

        System.out.println("=====分割线=====");
        myCircularQueue.enQueue(0);
        //原先rear在下标8的位置（我们new的是长度为9的数组，rear是队尾元素的后一个）
        //队尾再入一个元素，rear在下标0的位置
        System.out.println(myCircularQueue.rear);//打印0
        System.out.println(myCircularQueue.Rear());//得到队尾元素0
    }
}
