/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-01-16
 * Time: 15:27
 */
class Node{//链表实现
    public int val;
    public Node next;
    public Node(int val){
        this.val=val;
    }
}
public class MyQueue {
    public Node head;
    public Node last;
    public void offer(int val){//队尾添加一个元素——尾插
        Node node=new Node(val);
        if(head==null){
            head=node;
            last=node;
        }else {
            last.next=node;
            last=last.next;
        }
    }

    public int poll(){//队头出一个元素——头删
        if(isEmpty()){
            throw new RuntimeException("队列为空");
        }
        int oldVal=head.val;
        this.head=head.next;
        return oldVal;
    }

    public boolean isEmpty(){
        return this.head==null;
    }

    public int peek(){
        if(isEmpty()){
            throw new RuntimeException("队列为空");
        }
        return head.val;
    }

    public static void main(String[] args) {
        MyQueue myQueue=new MyQueue();
        myQueue.offer(1);
        myQueue.offer(2);
        myQueue.offer(3);
        myQueue.offer(4);
        System.out.println(myQueue.peek());//1
        System.out.println(myQueue.poll());//1
        System.out.println(myQueue.poll());//2
        System.out.println(myQueue.poll());//3
        System.out.println(myQueue.poll());//4
        System.out.println(myQueue.poll());//这里会报一个队列为空的警告
    }
}
