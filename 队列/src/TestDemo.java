import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-01-16
 * Time: 14:58
 */
public class TestDemo {

    //普通队列
    /*public static void main(String[] args) {
        Queue<Integer> queue=new LinkedList<>();//普通的队列——队尾进，队头出

        queue.add(1);
        queue.offer(2);//add和offer都是往队列里面放元素
        System.out.println(queue.peek());//peek是获取队头元素（不删除），这里打印1
        System.out.println(queue.element());//element也是获取队头元素，打印1
        System.out.println(queue.poll());//拿出一个元素（队列里面删除掉了），打印1
        System.out.println(queue.remove());//拿出一个元素（队列里面删除掉了），打印2
        System.out.println(queue.peek());//1和2全部删除掉了，打印null

        //具体使用时add和offer，poll和remove，peek和element等都可以，一般用offer，poll，peek更多
        //add和offer的区别就是：当队列容量max时，再往里面添加元素，add会抛异常，offer不会
        //其余两组也是抛不抛异常的区别，实际影响不大

    }*/

    //双端队列
    public static void main(String[] args) {
        Deque<Integer> deque=new LinkedList<>();//双端队列——队尾队头都能进，都能出
        //双端队列也可以当成栈来使用
        deque.offer(1);//默认从队尾入
        deque.offerFirst(2);//从队头入
        System.out.println(deque.peek());//打印2
        System.out.println(deque.peekFirst());//打印2
        System.out.println(deque.peekLast());//打印1
    }

    //ArrayList和LinkedList的区别：
    //1.共性，增删查改
    //2.前者是顺序表，后者是链表

}
