/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-12-03
 * Time: 13:46
 */
/*public class MyArrayList {//我们这里实现一个顺序表
    //1.普通顺序表
    *//*public int []elem;
    public int usedSize;

    public MyArrayList(){
        this.elem=new int[10];
        this.usedSize=0;
    }

    public void add(int val){//我们默认在数组最后添加数据
        this.elem[usedSize]=val;
        usedSize++;
    }

    public int getVal(int pos){//获取下标pos的数据
        return this.elem[pos];
    }*//*

    //2.可以添加其他类型数据的顺序表
    *//*public Object []elem;
    public int usedSize;

    public MyArrayList(){
        this.elem=new Object[10];
        this.usedSize=0;
    }

    public void add(Object val){//我们默认在数组最后添加数据
        this.elem[usedSize]=val;
        usedSize++;
    }

    public Object getVal(int pos){//获取下标pos的数据
        return this.elem[pos];
    }*//*
}*/


//法3，泛型法
    //ps:泛型的坑
    //1.不能new泛型类型的数组
    //1解释：数组在运行时存储和检查类型信息，然而，泛型在编译时检查类型错误，并且在运行时没有类型信息——换句话说，你new一个泛型的，我们不知道泛型里面存放什么类型，编译器无法给你开辟具体大小的空间
    //2.简单类型不能作为泛型类型的参数，比如int、char、float

    //ps:泛型的好处
    //1.可以自动进行类型的检查，比如你<Integer>里面只能存储int，存其他的类型数据，比如String就会报错
    //2.可以自动进行类型转换，比如你传过去int类型的（<>里的是Integer），get过来就不用再强转了

    //面试问题：泛型到底是怎么编译的？
    //答案：擦除机制，指的是，在编译阶段，泛型类型被擦除(也就是<>里的内容)，变为Object了
    //！！！注意，擦除不是替换，擦除不是替换，擦除不是替换
    //因为是擦除，所有你可以放Integer、String。。。
    //举个例子：你在MyArrayList<Integer> myArrayListInt，那么MyArrayList里面的public T[] elem;在编译的时候，T变成了Object，你可以放int类型的




    //！！！泛型只是编译时期的一种机制（擦除机制），在运行的时候不存在泛型的说法，所有的处理均在编译阶段处理结束
/*public class MyArrayList <T>{//<T>只是一个占位符，表示当前类是一个泛型类，你也可以用<E>
    public T[] elem;
    public int usedSize;
    public MyArrayList(){
        //this.elem=new T[10]错误！，不能new泛型类的数组——将来你接收的时候，因为是T擦除变成了Object类型
        //也就是说，T里面什么类型都可能拥有，你接收用什么类型呢？在你接收的时候，编译器会报错


        this.elem=(T[])new Object[10];//可以创建一个Object类型的数据，然后把它强转成泛型类的数组
        this.usedSize=0;
    }
    public void add(T val){//这里就用T类型创建数据了（你要添加到T[]数组里面嘛）
        this.elem[usedSize]=val;
        usedSize++;
    }
    public T getVal(int pos){
        return elem[pos];
    }
}*/


public class MyArrayList <E>{//常用的通配符有T,E,K,V,N,T分别表示类型、元素、键、值、number、type
    //通配符不用做太多区分，只是方便自己使用而已，下面用E的地方，你用别的字母也没有关系
    //这里的通配符就相当于形参，后面调用的时候比如里面放字符串，用<String>，String就是形参
    public E[] elem;
    public int usedSize;
    public MyArrayList(){
        this.elem=(E[])new Object[10];
        this.usedSize=0;
    }
    public void add(E val){
        this.elem[usedSize]=val;
        usedSize++;
    }
    public E getVal(int pos){
        return elem[pos];
    }


}
