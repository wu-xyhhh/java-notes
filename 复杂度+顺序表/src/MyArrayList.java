import java.util.Arrays;
//如果需要访问一个函数，看它内部构造，鼠标放在函数上，ctrl+左键
/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-11-09
 * Time: 15:30
 */
public class MyArrayList {
    public int[] elem;//存储数据的数组
    public int usedSize;//默认为0
    public MyArrayList(){
        this.elem=new int[10];//直接在上面赋值也可以
    }

    //打印顺序表
    public void display(){
        for(int i=0;i<this.usedSize;i++){
            System.out.print(this.elem[i]+" ");
        }
    }

    //获取顺序表长度
    public int size(){
        return this.usedSize;
    }

    //判断顺序表是否已满
    public  boolean isFull() {
        return this.usedSize==this.elem.length;//如果满了返回true，反之返回false
    }

    //在pos位置插入数据key
    public void add(int pos,int key){//在pos位置插入数据key
        if(pos<0||pos>this.usedSize){//你不能在顺序表最前面的前面插入或最后面的后面插入
            System.out.println("插入位置选择错误，请重新输入");
            return;
        }
        if(isFull()){//判断顺序表是否已满
            System.out.println("顺序表已满，正在进行扩容");
            this.elem=Arrays.copyOf(this.elem,2*this.elem.length);
            //拷贝函数，将原先数组元素拷贝到新数组里，新数组长度为第二个参数，然后返回新的数组

            return;
        }
        for(int i=this.usedSize-1;i>=pos;i--){//从后面开始移，如果你从前面移动，会把后面的数据覆盖掉
            //比如     1 2 3 4 （），现要在2这个位置插入一个元素k，2及以后的数向后移动，（）表示顺序表剩余位置
            //移动应该是1 2 3 4 4
            //        1 2 3 3 4
            //        1 2 2 3 4

            //        1 k 2 3 4
            this.elem[i+1]=this.elem[i];
        }
        this.elem[pos]=key;
        this.usedSize++;
    }


    //判断顺序表中是否存在某一元素
    public boolean contain(int data){
        for(int i=0;i<this.elem.length;i++){
            if(this.elem[i]==data){
                return true;
            }
        }
        return false;
    }

    //查找某个元素在顺序表中对应位置,找到返回下标，否则返回-1（数组没有负数下标）
    public int search(int pos){
        for(int i=0;i<this.usedSize;i++){
            if(this.elem[i]==pos){
                return i;
            }
        }
        return -1;
    }

    public boolean isEmpty(){//判断顺序表是否为空
        return this.usedSize==0;
    }

    //获取pos位置的值
    public int getPos(int pos){
        if (pos < 0||pos>=this.usedSize) {
            System.out.println("获取位置不合法，请重新确认位置");
            return -1;//业务上的处理我们不考虑，后期会学习抛异常
        }
        if(isEmpty()){
            System.out.println("顺序表为空");
        }
        return this.elem[pos];
    }

    //将pos位置的值，改成值value
    public void setPos(int pos,int value){
         if(pos<0||pos>=this.usedSize){
             System.out.println("pos位置不合法");
         }
         if(isEmpty()){
             System.out.println("顺序表为空！");
             return;
         }
         this.elem[pos]=value;
    }

    //删除指定位置元素
    public void remove(int data){
        if(isEmpty()){
            System.out.println("顺序表为空，无法进行删除操作");
        }
        int pos=search(data);//定义pos来接收指定元素下标
        if(pos==-1){//可能顺序表没有指定元素
            System.out.println("没有你要删除的数字");
            return;
        }
        //有所需数字，找到后就是挪数字(从后往前挪）
        //比如1 2 3 4 5 现要删除2
        //   1  3 4 5(3把2覆盖，4把3覆盖，5把4覆盖）
        for(int i=pos;i<this.elem.length;i++){
            //这里需要注意的是，i<length-1,因为你会访问i+1，如果i=length-1，i+1=length，elem[length]会造成危险访问
            this.elem[i]=this.elem[i+1];
        }
        //删除一个元素后，总元素个数-1
        this.usedSize--;//这些this你可以加也可以不加，习惯性还是加上好
        //如果数组元素是引用类型还需要以下代码
        //this.elem[usedSize]=null;
        //我们这里因为是整形为顺序表元素，我们usedSize--后就访问不到那个空间了，系统如果需要可以直接改
        //但如果我们的顺序表元素为引用变量（它会指向一个对象），我们usedSize--后访问不到引用变量，引用变量它就一直指向那个对象，而对象就一直占用空间
        // 系统无法回收那块空间（如果需要系统自动回收，只有一种情况就是整个顺序表被回收），你需要自己把它的指向置为null
    }

    //清空顺序表
    public  void clear() {
        this.usedSize=0;
    }
    public static void main(String[] args) {
        MyArrayList myArrayList=new MyArrayList();
        myArrayList.add(0,1);
        myArrayList.add(1,2);
        myArrayList.add(2,3);
        myArrayList.add(3,4);
        myArrayList.add(3,5);
        myArrayList.add(3,6);
        myArrayList.display();
        System.out.println();
        System.out.println(myArrayList.contain(1));//判断顺序表中是否存在元素1
        System.out.println(myArrayList.contain(7));
        System.out.println(myArrayList.getPos(3));//获取3下标的元素
        myArrayList.setPos(0,2);
        myArrayList.display();
        myArrayList.clear();//清空顺序表
        myArrayList.display();
    }
}
