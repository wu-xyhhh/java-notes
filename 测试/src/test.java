import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;

import static java.lang.Math.pow;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2021-12-26
 * Time: 12:21
 */


public class test {
    //例1
    public static int Fib(int n){//n是我们要变成斐波那契的数
        int x=0;
        int y=1;
        int tmp=0;

        while(Math.abs(n-x)>=Math.abs(n-y)){//循环条件不成立x,i,tmp整体向后移1位
            tmp=x+y;
            x=y;
            y=tmp;
        }

        //走完上面的循环，离n最近的i找到了
        return Math.abs(n-x);
    }

    public static void main0(String[] args) {
        Scanner in = new Scanner(System.in);
            int a = in.nextInt();
            int b=Fib(a);
            System.out.println(b);
    }

    public static void main1(String[] args) {
        Scanner in = new Scanner(System.in);
        HashSet<String> hashSet=new HashSet<>();
        while(in.hasNextLine()){
            hashSet.add(in.nextLine());
        }
        System.out.println(hashSet.size());
    }


    public static void main2(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int N= scanner.nextInt();
        int max=0;
        for(int i=1;i<=N;i++){
            int t=i;
            while(t!=1){
                if(t%2==0){
                    t=t/2;
                }else{
                    t=t*3+1;
                }
                if(t>=max){
                    max=t;
                }
            }
        }
        System.out.println(max);
    }

    public static void main3(String[] args) {
        int z=365/7;//一年多少周
        int y=365%7;//余下来几天（假设余下来的都算工作日)
        double factor=0.01;
        double end=1;//最终结果
        for(int i=0;i<z;i++){
            end*=pow(1+factor,5);
            end*=pow(1-factor,2);
        }
        end*=pow(1+factor,y);
        System.out.println(end);
    }


    public static void main(String[] args) {
        int []arr={2,34,1,6,8};
        Arrays.sort();
    }



}
