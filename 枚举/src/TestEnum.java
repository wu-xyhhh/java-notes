/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-03-02
 * Time: 21:26
 */
public enum TestEnum {
    RED,BLACK,GREEN,WHITE;

    //compareTo()比较两个枚举成员在定义时的顺序
    public static void main(String[] args) {//ps:我们自己写的枚举类都默认继承Enum类
        TestEnum testEnum=TestEnum.valueOf("RED");
        System.out.println(RED.compareTo(BLACK));//打印-1
        //打印值=RED-BLACK=0-1
        System.out.println(BLACK.compareTo(RED));//打印1
        System.out.println(BLACK.compareTo(WHITE));//打印-2
    }

    //valueOf()把字符串 变成对应的枚举对象
    public static void main4(String[] args) {
        TestEnum testEnum=TestEnum.valueOf("RED");
        //如果不是 RED,BLACK,GREEN,WHITE其中之一，运行时会报错
        System.out.println(testEnum);//打印RED
    }

    //ordinal():获取枚举成员的索引位置
    public static void main3(String[] args) {
        TestEnum[]testEnums=TestEnum.values();
        for(int i=0;i<testEnums.length;i++){
            System.out.println(testEnums[i]+"->"+testEnums[i].ordinal());
        }//打印RED->0 BLACK->1 GREEN->2 WHITE->3
    }

    //values()：以数组形式返回枚举类型的所有成员
    public static void main2(String[] args) {
        TestEnum[] testEnums=TestEnum.values();
        for(int i=0;i< testEnums.length;i++){
            System.out.println(testEnums[i]);
        }//打印RED BLACK GREEN WHITE
    }


    public static void main1(String[] args) {
        TestEnum testEnum=TestEnum.RED;
        switch (testEnum) {//会匹配到RED，打印red
            case RED:
                System.out.println("red");
                break;
            case BLACK:
                System.out.println("black");
                break;
            case WHITE:
                System.out.println("WHITE");
                break;
            default:
                break;
        }
    }




    public static void main0(String[] args) {
        System.out.println(TestEnum.RED);//打印RED
        System.out.println(RED);//打印RED
    }
}
