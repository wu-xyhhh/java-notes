/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 25397
 * Date: 2022-03-02
 * Time: 22:13
 */
public enum TestEnum1 {
    RED("red",1),BLACK("black",2),GREEN,WHITE;
    public String color;
    public int ordinal;

    TestEnum1(String color,int ordinal){
        this.color=color;
        this.ordinal=ordinal;
    }

    private TestEnum1(){//这个构造方法你不写也是默认的,
        // ps:private也是默认的（这里可以不写private），枚举也是不可以被继承的

    }

}
